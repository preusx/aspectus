import './index.sass';

import { extend, ValidationObserver } from 'vee-validate';
import {
  required, min, min_value, max, max_value
} from 'vee-validate/dist/rules';

import VueTag from '@aspectus/vue-tag/src/main';
import VueDropzone from '@aspectus/vue-dropzone/src/main';
import VueSelectionController from '@aspectus/vue-selection-controller/src/main';
import VueAllSelectionController from '@aspectus/vue-all-selection-controller/src/main';
import VuePermissions from '@aspectus/vue-permissions/src/main';
import VueDropzoneUploader from '@aspectus/vue-dropzone-uploader/src/main';

import VueGrid from '@aspectus/vue-grid/src/main';
import VueDermis from '@aspectus/vue-dermis/src/main';
import VueControlLabel from '@aspectus/vue-control-label/src/main';
import VueControlHint from '@aspectus/vue-control-hint/src/main';
import VueControlDescriptor from '@aspectus/vue-control-descriptor/src/main';
import VueControlInteractionStateController from '@aspectus/vue-control-interaction-state-controller/src/main';

import VeeOptionalProvider from '@aspectus/vee-optional-provider/src/main';
import VeeControlDescriptor from '@aspectus/vee-control-descriptor/src/main';

import { Permission, Or, And } from '@aspectus/permissions';

import { resource } from './resources';

// `Promise.any` polyfill is required for `Or` checker.
const reverse = p => new Promise((resolve, reject) => Promise.resolve(p).then(reject, resolve));
Promise.any = arr => reverse(Promise.all(arr.map(reverse)));
// polyfill-end

class AlwaysTrue extends Permission { }
class AlwaysFalse extends Permission {
  hasAccess() {
    return false;
  }
}

extend('required', { ...required, message: 'This is required' });
extend('min', min);

export default ({ Vue }) => {
  global.Vue = Vue;

  Vue.prototype.$testPermissions = {
    Or, And, AlwaysTrue, AlwaysFalse,
  };
  Vue.prototype.$testResources = {
    uploadResource: resource.config('response', () => ({
      id: Date.now(),
      name: 'Uploaded_file_name.txt',
    })),
  };

  Vue.component('vee-validation-observer', ValidationObserver);

  Vue.use(VueTag);
  Vue.use(VueDropzone);
  Vue.use(VueSelectionController);
  Vue.use(VueAllSelectionController);
  Vue.use(VuePermissions);
  Vue.use(VueDropzoneUploader);

  Vue.use(VueGrid);
  Vue.use(VueDermis);
  Vue.use(VueControlLabel);
  Vue.use(VueControlHint);
  Vue.use(VueControlDescriptor);
  Vue.use(VueControlInteractionStateController);

  Vue.use(VeeOptionalProvider);
  Vue.use(VeeControlDescriptor);
};
