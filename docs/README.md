---
lang: en-US
home: true
heroImage: /logo.svg
actionText: Get started »
actionLink: /packages/bem.html
footer: MIT Licensed | Copyright © 2019-present Alex Tkachenko
meta:
  - name: og:title
    content: Aspectus
  - name: og:description
    content: Set of tools for application development
---
