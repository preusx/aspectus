# Cancellable fetch API

Composable fetch API interface.

## Installation

```
yarn add @aspectus/cancellable-fetch
```

## Usage

Fetch call will return cancellable promise with additional `.cancel` method(from `@aspectus/cancellable-promise`), that you may execute at any time you'll need.

```js
import fetch from '@aspectus/cancellable-fetch';

fetch('http://googl.com').cancel();
```
