# Cancellable Promise proxy

Simple Promise extension to use `AbortController` to cancel underlying process.

## Installation

```
yarn add @aspectus/cancellable-promise-proxy
```

## Usage

For example to make cancellable fetch:

```js
import cancellablePromiseProxy from '@aspectus/cancellable-promise-proxy';

const cancelController = new AbortController();
const promise = cancellablePromiseProxy(
  new Promise((resolve, reject) => {
    fetch('http://google.com', { signal: cancelController.signal })
      .then(resolve, reject);
  }),
  { cancelController }
);

promise.cancel();
```

Method `.cancel` is available after any amount of `.then()` and `.catch()` calls. It will call controller's `.abort` method.
