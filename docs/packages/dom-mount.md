# DOM Mount

Mechanism to made mounting custom js scripts more easy.

## Installation

```
yarn add @aspectus/dom-mount
```

## Usage

```js
import { MountRegistry } from '@aspectus/dom-mount';

const registry = new MountRegistry('js:mounting-event-name');

const customScript = domElementsContext => {
  const elements = domElementContext.querySelectAll('.js-some-root-element');
  // ... some stuff to do with elements
};

registry.register([customScript]);

const element = document.createElement('div');

registry.mount(document); // `domElementsContext` will be `document`
registry.mount(element); // `domElementsContext` will be `element`
```
