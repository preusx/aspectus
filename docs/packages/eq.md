# Element queries `eq` observer

Implementation of the element queries observer.

## Installation

```
yarn add @aspectus/eq
```

## Usage

```js
import { EqObserver } from '@aspectus/eq';

const element = document.body;
const observer = new EqObserver({
  // Breakpoints on which relevant classes will be added.
  // Default breakpoints are:
  // 'gte260', 'lt260',
  // 'gte390', 'lt390',
  // 'gte520', 'lt520',
  // 'gte780', 'lt780',
  // 'gte1040', 'lt1040',
  breakpoints: ['gt1000', 'lt900', '>=500']
});

// Bind element to update classes.
// May bind as many elements as you want.
observer.bind(element);

// Need to update current breakpoints
observer.update({ breakpoints: ['<=500'] });

// Unbinds a particular element.
observer.unbind(element);

// Destroys observer at all, while unbinds all binded elements.
observer.clear();
```
