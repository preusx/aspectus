# Permissions

Basement for permissions checking functionality.

## Installation

```
yarn add @aspectus/permissions
```

## Usage

Define your own permissions checkers and use them.


```js
import { Permission, Or, And } from '@aspectus/permissions';

// `Promise.any` polyfill is required for `Or` checker.
const reverse = p => new Promise((resolve, reject) => Promise.resolve(p).then(reject, resolve));
Promise.any = arr => reverse(Promise.all(arr.map(reverse)));
// polyfill-end

class AlwaysTrue extends Permission { }

class AlwaysFalse extends Permission {
  hasAccess() { return false; }
}

class PassedReturn extends Permission {
  hasAccess(value) { return value; }
}

class IsAuthenticated extends Permission {
  hasAccess(user) { return user.isAuthenticated; }
}

class HasPermission extends Permission {
  constructor(permission) {
    super();
    this.permission = permission;
  }

  hasAccess(user) { return user.permissions.includes(this.permission); }
}

// First permission checker
const resolvable = new Or(
  new And(
    new Or(
      new AlwaysTrue(),
      new AlwaysFalse()
    ),
    new Or(
      new AlwaysFalse(),
      // You should pass `true` if you want to resolve this checker
      new PassedReturn()
    )
  ),
  new Or(
    new AlwaysFalse(),
    new And(new AlwaysTrue(), new AlwaysFalse())
  )
);

// Permission checker, that will always reject
const rejectable = new Or(
  new And(
    new Or(
      new AlwaysTrue(),
      new AlwaysFalse()
    ),
    new Or(
      new AlwaysFalse(),
      new And(new AlwaysTrue(), new AlwaysFalse())
    )
  ),
  new Or(
    new AlwaysFalse(),
    new And(new AlwaysTrue(), new AlwaysFalse())
  )
);

// Authentication checker example
const isAuthenticated = new IsAuthenticated();
const canEdit = new HasPermission('edit');
const canSummon = new HasPermission('summon');
const authenticatedOrEdit = new Or(isAuthenticated, canEdit);

resolvable.onHasAccess(true).then(() => console.log('Successfully resolved.'));
resolvable.onHasAccess(false).catch(() => console.log('Checker failed.'));
rejectable.onHasAccess().catch(() => console.log('Checker failed.'));

isAuthenticated.onHasAccess({ isAuthenticated: false })
  .catch(() => console.log('User checking failed.'));
isAuthenticated.onHasAccess({ isAuthenticated: true })
  .then(() => console.log('User checking succeeded.'));

canEdit.onHasAccess({ permissions: ['edit', 'summon'] })
  .then(() => console.log('Yeah, he can'));
canSummon.onHasAccess({ permissions: ['edit', 'summon'] })
  .then(() => console.log('Yeah, he can'));
authenticatedOrEdit.onHasAccess({ permissions: ['edit', 'summon'] })
  .then(() => console.log('Yeah, he can'));
authenticatedOrEdit.onHasAccess({ isAuthenticated: true, permissions: ['summon'] })
  .then(() => console.log('Yeah, he can'));

authenticatedOrEdit.onHasAccess({ permissions: ['summon'] })
  .catch(() => console.log('Nope, he can not'));
canSummon.onHasAccess({ permissions: ['edit'] })
  .catch(() => console.log('Nope, he can not'));
```
