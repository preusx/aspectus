# Progress Promise proxy

Simple Promise proxy that adds `.progress` handler registry to promise object.

## Installation

```
yarn add @aspectus/progress-promise-proxy
```

## Usage

```js
import progressPromiseProxy from '@aspectus/progress-promise-proxy';
import EventEmitter from '@aspectus/event-emitter';

const progressEmitter = new EventEmitter();
const promise = progressPromiseProxy(
  new Promise((resolve, reject) => {
    setTimeout(() => resolve(100), 1000);
    setTimeout(() => progressEmitter.emit('progress', 50), 500);
  }),
  { progressEmitter }
);

promise.then(console.log).progress(console.log);
// > 50
// > 100
```

Method `.progress` is available after any amount of `.then()` and `.catch()` calls. It will add `progress` listener to provided event emitter.
