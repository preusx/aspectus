# Template url getter

Template url getter function for `@aspectus/resource`.

## Installation

```
yarn add @aspectus/resource-template-url-getter
```

## Usage

```js
import getter from '@aspectus/resource-template-url-getter';
import { receiveResource } from '@aspectus/resource';

const googleRequestResource = receiveResource.url(getter('http://google.com{?q}'));
```
