# Fetch API adapter for superagent usage

Custom fetcher that adapts [superagent](https://github.com/visionmedia/superagent) library to the default Fetch API.

## Installation

```
yarn add @aspectus/superagent-fetch
```

## Usage

Fetch call will return superagent promise with additional `.cancel` and `.progress` methods(from `@aspectus/cancellable-promise-proxy` and `@aspectus/progress-promise-proxy`), that you may execute at any time you'll need.

```js
import fetcher from '@aspectus/superagent-fetch';

fetcher('http://googl.com').cancel();
```
