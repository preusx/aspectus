# Vee control descriptor

Control descriptor integrated with vee validate provider. Special wrapper for control element that adds additional description.

## Installation

```
yarn add @aspectus/vee-control-descriptor
```

```js
import VeeControlDescriptorPlugin from '@aspectus/vee-control-descriptor';

Vue.use(VeeControlDescriptorPlugin);
```

## Elements

Plugin adds several components:

- `vee-control-descriptor` - Descriptor itself.
- `vee-control-descriptor-controller-interaction-hide-errors` - Controller that uses [vue-control-interaction-state-controller](vue-control-interaction-state-controller.html) to hide errors on user field interaction. This is default one.
- `vee-control-descriptor-controller-none` - Controller that done nothing(it's here, because you may need to remove default behavior).

::: warning No styling!
This package does not provide any styling, so you should add the base styling by your own.
:::

### `vee-control-descriptor`

It wraps [vue-control-descriptor](vue-control-descriptor.html) with `ValidationProvider` from `vee-validate` package, and adds small additional logic to automatically display errors.

#### Props:

| Name | Description |
| --- | --- |
| `descriptorComponent` | Descriptor component - by default it's [vue-control-descriptor](vue-control-descriptor.html). |
| `providerComponent` | `ValidationProvider` component by default, or some component with the same API(extended, maybe). |
| `controllerComponent` | Component that lays between `providerComponent` and `descriptorComponent`. By default - `vee-control-descriptor-controller-interaction-hide-errors`.|

## Example

::: demo
```html
<template>
  <div>
    <vee-control-descriptor
      element="input"
      rules="required"
      placeholder="Input, that you need to fill"
      label-text="Label"
      hint-text="Small hint for the control description."
      v-model="model"
    />

    <vee-control-descriptor
      class="someclass"
      :class="'otherclass'"
      element="input"
      rules="required"
      placeholder="Input, that you need to fill"
      label-text="Label"
      hint-text="Small hint for the control description."
      controller-component="vee-control-descriptor-controller-none"
      v-model="model"
    />
  </div>
</template>
<script>
export default {
  data() { return { input: '' }; },
  computed: {
    model: {
      get() { return this.input },
      set(e) { this.input = e.target.value },
    },
  },
};
</script>
```
:::
