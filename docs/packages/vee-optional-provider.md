# Vee optional provider

Vee validate component that renders `ValidationProvider` only when `rules` prop is not empty.

## Installation

```
yarn add @aspectus/vee-optional-provider
```

```js
import OptionalProviderPlugin from '@aspectus/vee-optional-provider';

Vue.use(OptionalProviderPlugin);
// Or
Vue.use(OptionalProviderPlugin, { name: 'v-optional-provider' });
```

## Usage

Use in your component instead of default ValidationProvider component.

::: demo
```html
<template>
  <div>
    <div>There should be no "[ERROR]" there:</div>
    <vee-optional-provider v-slot="{ valid }" slim>
      1{{ typeof valid == 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim rules="">
      2{{ typeof valid == 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim :rules="{}">
      3{{ typeof valid == 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim :rules="null">
      4{{ typeof valid == 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim :rules="undefined">
      5{{ typeof valid == 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim :rules="[]">
      6{{ typeof valid == 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim rules="required">
      7{{ typeof valid !== 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" slim :rules="{ required: true }">
      8{{ typeof valid !== 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
    <vee-optional-provider v-slot="{ valid }" :rules="{ required: true }">
      9{{ typeof valid !== 'undefined' ? '' : '[ERROR]' }}
    </vee-optional-provider>
  </div>
</template>
<script>
export default {};
</script>
```
:::
