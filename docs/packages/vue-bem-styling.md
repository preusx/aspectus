# Vue BEM styling

Set of vue utilities to make blocks styling easier.

## Installation

```
yarn add @aspectus/vue-bem-styling
```

## Usage

### extractModifiers(props: object)

Function that picks default modifiers from `props` object. And transforms the names to a styling-matched modifier names.

### extractStates(props: object)

Same as `extractModifiers` but for state names.

### StylingPropsMixin

Mixin that adds all styling default named props to a component.

### StylingMixin

Mixin for stateful components. Adds `modifiers` and `states` computed properties. And methods `extractModifiers` and `extractState` for their extraction.

## Naming

[Тут](https://preusx.github.io/dermis/ru/modules/naming.html).
