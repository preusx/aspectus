# Vue BEM implementation

BEM directives for vue.

## Installation

```
yarn add @aspectus/vue-bem
```

```js
import BemPlugin from '@aspectus/vue-bem';

Vue.use(BemPlugin);
// Or
Vue.use(BemPlugin, {
  block: 'bem-directive-name',
  state: 'state-directive-name',
  blockNameKey: 'key in component $options where block name locates',
  naming: {
    n: 'Namespace. `` by default',
    e: 'Element delimiter. `__` by default',
    m: 'Modifier delimiter. `--` by default',
    v: 'Modifier value delimiter. `_` by default',

    sp: 'State prefix. `is-` by default',
  },
});
```

## Usage

### `v-bem`

```html
<template>
  <div v-bem:block-name="{ modifier: true }">
    <div v-bem:block-name.element-name="{ modifier: true }">
    </div>
  </div>
</template>
```

Or if component has block name:

```html
<template>
  <div v-bem="{ modifier: true }">
    <div v-bem.element-name="{ modifier: true }">
    </div>
  </div>
</template>

<script>

export default {
  name: 'block-name',
  // Or
  block: 'block-name',
  // Or
  blockName: 'block-name',
  // Or
  [blockNameKey]: 'block-name',
};

</script>
```

### `v-state`

```html
<template>
  <div v-state.hidden.visible.disabled="{ opened: true }"></div>
</template>
```

