# Vue `control-descriptor` component

Control descriptor component. Special wrapper for control element that adds additional description.

## Installation

```
yarn add @aspectus/vue-control-descriptor
```

```js
import ControlDescriptorPlugin from '@aspectus/vue-control-descriptor';

Vue.use(ControlDescriptorPlugin);
```

## Elements

Plugin adds two components:

- `control-descriptor` - Which is main component.
- `control-descriptor-layout` - Default layout(which can be easily changed).

::: warning No styling!
This package does not provide any styling, so you should add the base styling by your own.
:::

### `control-descriptor`

#### Props:

| Name | Description |
| --- | --- |
| `layoutComponent` | Component to use as a base layout. |
| `labelComponent` | Component to use for label display. |
| `hintComponent` | Component to use to display hint elements. |
| `errorComponent` | Component to use to display error elements. |
| `layoutProps` | Object with props to pass to a layout component. |
| `tag` | Root tag that will be passed to layout-component. |
| `element` | Control element that you want to wrap. <br/> All other passed events and props will be attached to this element. |
| `labelText` | Text for descriptor label. |
| `hintText` | Text for descriptor hint. |
| `disabled` | Disabled state checker |
| `required` | Required state checker |
| `readonly` | Readonly state checker |
| `errors` | List of errors(string) to display inside |

#### Slots:

All the slots should be also supported by the passed `layout-component`. Any other not listed slot will be passed into the `element` component. If `element` component has same named slots just prefix them with `'element-'` prefix and they will be resolved in the right way.

| Name | Description |
| --- | --- |
| `label` | Placeholder to replace default `label` element rendering. |
| `hint` | Placeholder to replace default `hint` element rendering. |
| `errors` | Placeholder to replace default `errors` rendering. |
| `element` | Placeholder to replace default `element` rendering. |
| `meta` | Slot for rendering additional `meta` information. Doesn't implemented by default. |

### `control-descriptor-layout`

Simple layout component for `control-descriptor`. It determines descriptor's markup and block classes.

#### Props:

| Name | Description |
| --- | --- |
| `blockName` | Name of the component's BEM block. |

Available all additional styling props [from `@aspectus/vue-bem-styling`](vue-bem-styling.html).

#### Slots:

All slots are just an empty placeholders, nothing more. All the slots are wrapped in `transition` element and are renders only if slot was passed in.

| Name | Description |
| --- | --- |
| `label` | Slot for rendering `label`. |
| `hint` | Slot for rendering `hint`. |
| `errors` | Slot for rendering `errors`. |
| `element` | Slot for rendering `element`. |
| `meta` | Slot for rendering additional `meta` information. |

## Examples

### Simple standalone input component

::: demo
```html
<template>
  <div>
    <control-descriptor
      element="input"
      v-model="model"
    />
  </div>
</template>
<script>
export default {
  data() { return { input: '' }; },
  computed: {
    model: {
      get() { return this.input },
      set(e) { this.input = e.target.value },
    },
  },
};
</script>
```
:::

### Input "full packed" component

::: demo
```html
<template>
  <div>
    <control-descriptor
      element="input"
      label-text="Full packed input"
      hint-text="Nothing to do with him, just watch."
      :errors="['Error one', 'Error two']"
      v-model="model"
    >
      <template #meta>Even with meta: input's value is "{{ input }}"</template>
    </control-descriptor>
  </div>
</template>
<script>
export default {
  data() { return { input: '' }; },
  computed: {
    model: {
      get() { return this.input },
      set(e) { this.input = e.target.value },
    },
  },
};
</script>
```
:::

### Element's slots

Even `control-descriptor` inside `control-descriptor` is possible.

::: demo
```html
<template>
  <div>
    <control-descriptor
      element="control-descriptor"
      label-text="Element's slots example"
      hint-text="Nothing to do with him, just watch."
      :errors="['Error one', 'Error two']"
      v-model="model"
    >
      <template #element-meta>Even with meta: input's value is "{{ input }}"</template>
      <template #element-element="slotProps">
        <input
          v-bind="slotProps.attrs"
          :value.prop="slotProps.value"
          :disabled="slotProps.disabled"
          :required="slotProps.required"
          :readonly="slotProps.readonly"
          :invalid="slotProps.invalid"
          v-on="slotProps.listeners"
        />
      </template>
    </control-descriptor>
  </div>
</template>
<script>
export default {
  data() { return { input: '' }; },
  computed: {
    model: {
      get() { return this.input },
      set(e) { this.input = e.target.value },
    },
  },
};
</script>
```
:::
