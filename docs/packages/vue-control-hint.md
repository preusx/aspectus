# Vue control hint

Simple functional component for `control-hint` element.

## Installation

```sh
yarn add @aspectus/vue-control-hint
```

## Usage

```js
import HintPlugin from '@aspectus/vue-control-hint';

Vue.use(HintPlugin);
// Or
Vue.use(HintPlugin, { name: 'c-hint' });
```

::: demo
```html
<template>
  <div>
    <control-hint>Hint</control-hint>
    <control-hint styling="default" variant="hint">Hint with variant</control-hint>
    <control-hint styling="default" required disabled kind="singular" theme="dark">Hint with states and modifiers</control-hint>
    <control-hint variant="error" styling="default">Errors are also created with hint elements</control-hint>
  </div>
</template>
<script> export default {}; </script>
```
:::
