# Vue control interaction state controller

Component for handling state of user's interaction with some control element.

## Installation

```
yarn add @aspectus/vue-control-interaction-state-controller
```

```js
import ControlInteractionStateController from '@aspectus/vue-control-interaction-state-controller';

Vue.use(ControlInteractionStateController);
```

## Example

::: demo
```html
<template>
  <div>
    It uses @blur and @focus handlers to detect interaction. If your component uses some other events - just map `handlers` appropriately.

    <vue-control-interaction-state-controller
      @focus="inFocus=true"
      @blur="inFocus=false"
      v-model="model"
      v-slot="interactionState"
    >
      <div>
        <input
          :value.prop="interactionState.value"
          v-on="interactionState.handlers"
        />
      </div>
      <div>Value: "{{ interactionState.value }}"</div>
      <div>Interacted: "{{ interactionState.interacted }}" - If user had interacted with input at least once.</div>
      <div>Interacting: "{{ interactionState.interacting }}" - If user is currently interacting with element.</div>
      <div>Changed: "{{ interactionState.changed }}" - If the value was changed with this control at least once.</div>
      <div>Changing: "{{ interactionState.changing }}" - If after interaction start user had changed value and interaction isn't finished yet.</div>

      <div>In focus: {{ inFocus }}</div>
    </vue-control-interaction-state-controller>
  </div>
</template>
<script>
export default {
  data() { return { input: '', inFocus: false, }; },
  computed: {
    model: {
      get() { return this.input },
      set(e) { this.input = e.target.value },
    },
  },
};
</script>
```
:::
