# Vue Element queries implementation

EQ directive for vue. It is better to make use of `@dermis/eq` library, or
you should write additional classes by your own.

## Installation

```
yarn add @aspectus/vue-eq -D
```

## Usage

```js
import Plugin from '@aspectus/vue-eq';

// Just install.
Vue.use(Plugin);

// Use with different name(eq - by default).
Vue.use(Plugin, { name: 'u-eq' });

// May be used with a different default breakpoints:
Vue.use(Plugin, { defaultBreakpoints: ['gt1'] });
// Default breakpoints are:
// 'gte260', 'lt260',
// 'gte390', 'lt390',
// 'gte520', 'lt520',
// 'gte780', 'lt780',
// 'gte1040', 'lt1040',
```

And inside your component you just apply `v-eq` directive.

```vue
<template>
  <div class="some-component" v-eq />
</template>
```

Sometimes it's necessary to use a different breakpoints. In that case - directive's value to the rescue:

```vue
<template>
  <div class="other-component" v-eq="['gt100', 'lte100']" />
</template>
```
