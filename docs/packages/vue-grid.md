# Vue `grid` component

Dermis grid turned into vue components.

## Installation

```
yarn add @aspectus/vue-grid
```

Package is dependent on `vue-tag` cause all components are using it internally.

```js
import TagPlugin from '@aspectus/vue-tag';
import GridPlugin from '@aspectus/vue-grid';

Vue.use(TagPlugin);

Vue.use(GridPlugin);
// Or
Vue.use(GridPlugin, {
  rowName: 'custom-row-component-name',
  cellName: 'custom-cell-component-name',
  containerName: 'custom-container-component-name',
});
```

## Usage

As simple as that:

```html
<!-- SomeComponent -->
<template>
  <container fit="md">
    <row tag="span" space="md" align="center">
      <cell cols="12 4-md" align="end">Cell content</cell>
      <cell cols="12 4-md">Other cell content</cell>
      <cell cols="12 4-md">Third cell content</cell>
    </row>
  </container>
</template>
```

Dermis grid's blocks modifiers may be passed to all components as a props - and they will be applied.

Special prop `cols` on `cell` component will be transformed into a `g-cols` block with corresponding size modifiers.
