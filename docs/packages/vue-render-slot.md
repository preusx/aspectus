# Vue `render-slot` component

Functional component for rendering any scoped slot.

## Installation

```
yarn add @aspectus/vue-render-slot
```

```js
import RenderSlotPlugin from '@aspectus/vue-render-slot';

Vue.use(RenderSlotPlugin);
// Or
Vue.use(RenderSlotPlugin, { name: 'custom-render-slot' });
```

## Usage

Use in your component as a root element to be able to change it's tag on the fly.

```html
<!-- SomeComponent -->
<template>
  <component-that-has-logic-and-provide-renderable-slot-nodes v-slot="{ slots }">
    <!-- Layout for the logic, but renderless component -->

    <div><render-slot :slot="slots.header" /></div>

    <ul>
      <li v-for="count in [1,2,3,4,5]">
        <!-- You may pass props to a rendered slot -->
        <render-slot :slot="slots.element" :props="{ count }" />
      </li>
    </ul>

  </component-that-has-logic-and-provide-renderable-slot-nodes>
</template>
```
