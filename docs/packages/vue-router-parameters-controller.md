# Vue router parameters change controller

Utility component, that handles current route parameters change and url synchronization. Useful, for example, for a page filtering mechanism.

## Installation

```
yarn add @aspectus/vue-router-parameters-controller
```

Plugin adds a single `router-parameters-controller` component.

```js
import Plugin from '@aspectus/vue-router-parameters-controller';

Vue.use(Plugin);
// Or provide a different component's name
Vue.use(Plugin, { name: 'r-controller' });
```

## Usage

```js
<router-parameters-controller
  :initial="{ initial: 'parameter' }"
  :from="parametersFromRouteTransformer"
  :to="parametersToRouteTransformer"
  @change="everyRouteChangeHandler"
  @change:initial="initialRouteParametersHandler"
  @change:internal="onlyInternalParametersChangeHandler"
  @change:external="onlyExternalRouteChangeHandler"
  v-slot="{
    parameters, changeParameters, updateUrl
  }"
>
</router-parameters-controller>
```

### Slot props

- `parameters` - Current parameters.
- `changeParameters` - Method to change parameters. May be used filter to manipulate url.
- `updateUrl` - Manual url update method. In fact `changeParameters` is the same for now, but this one is exposed for future API usage.

### Props

- `initial` - Initial parameters to use if there is no alternative provided by current route.
- `from` - Transformer function to transform router received parameters to application-fittable parameters transformation.
- `to` - Transformer function for provided parameters to router-fittable parameters transformation.

### Events

- `@change` - Fires on every route change.
- `@change:initial` - Fires only at the first time route appears.
- `@change:internal` - Internal event. Fires on `changeParameters` method execution.
- `@change:external` - Occurs if any other process had changed the route. For example default router's `<router-link />` or a history change.
