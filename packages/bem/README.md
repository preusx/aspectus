# Utilities for `bem` classes creation

Set of function utilities to make bem classes generation easier.

[Documentation](https://preusx.gitlab.io/aspectus/packages/bem.html)
