import { generateModifierClassNames } from './block';
import { STATE_PREFIX, VALUE_DELIMITER } from './const';

export class StateGenerator {
  constructor({ sp = STATE_PREFIX, v = VALUE_DELIMITER } = {}) {
    this.config = { sp, v };
  }

  states(states = {}) {
    return generateModifierClassNames(this.config.sp, states, '', this.config.v);
  }
}

export function createStateGenerator(config) {
  const generator = new StateGenerator(config);

  return generator.states.bind(generator);
}
