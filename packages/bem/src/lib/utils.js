import flatten from 'ramda/src/flatten';

export const { isArray } = Array;
export const isString = value => toString.call(value) === '[object String]';
export const toArray = value => (isArray(value) ? value : [value]);

export function preparePairs(pairs) {
  return Object.keys(pairs || {})
    .reduce((acc, key) => {
      flatten(toArray(pairs[key]), Infinity)
        .filter(value => value)
        .forEach(value => acc.push(value === true ? [key] : [key, value]));
      return acc;
    }, []);
}
