# Cancellable fetch API

Composable fetch API interface.

[Documentation](https://preusx.gitlab.io/aspectus/packages/cancellable-fetch.html)
