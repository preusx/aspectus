# Cancellable Promise proxy API

Simple Promise proxy to use `AbortController` to cancel underlying process.

[Documentation](https://preusx.gitlab.io/aspectus/packages/cancellable-promise-proxy.html)
