import { createProxy } from '@aspectus/promise-proxy';

export default createProxy({
  cancel() {
    if (this.$data.cancelController && this.$data.cancelController.abort) {
      return this.$data.cancelController.abort();
    }

    return false;
  },
});
