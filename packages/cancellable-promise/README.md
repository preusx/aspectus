# Cancellable Promise API

Simple Promise extension to use `AbortController` to cancel underlying process.

[Documentation](https://preusx.gitlab.io/aspectus/packages/cancellable-promise.html)
