import { createEventHandler } from './utils';

export default class MountRegistry {
  constructor(event) {
    if (!event) {
      throw new Error('Event name is required.');
    }

    this.event = event;
  }

  register(components) {
    document.addEventListener(this.event, createEventHandler(components));

    return this;
  }

  mount(context) {
    document.dispatchEvent(new CustomEvent(this.event, { detail: { context } }));

    return this;
  }
}
