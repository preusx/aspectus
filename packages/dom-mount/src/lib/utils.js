export function mountComponents(components, context) {
  components.forEach(component => (component.mount || component)(context));
}

export function createEventHandler(components) {
  return function handler(event) {
    const { context } = event.detail;

    mountComponents(components, context);
  };
}
