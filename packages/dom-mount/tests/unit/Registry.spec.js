/* eslint-disable no-return-assign */

import { MountRegistry } from '@/main';

describe('Mounting', () => {
  it('Creates', () => {
    expect(() => new MountRegistry()).toThrow();
    expect(() => new MountRegistry('event-name')).not.toThrow();
  });

  it('Registers', () => {
    const mountingPoint = new MountRegistry('event-name');
    const mounter = () => {};

    expect(() => mountingPoint.register([])).not.toThrow();
    expect(() => mountingPoint.register([mounter])).not.toThrow();
  });

  it('Executes with proper context', () => {
    let context = null;
    let count = 0;
    const mountingPoint = new MountRegistry('event-name');
    const checker = c => (count += expect(c).toBe(context) || 1);

    mountingPoint.register([]);
    mountingPoint.register([checker]);

    document.body.innerHTML = '<div><div id="some"/></div>';

    mountingPoint.mount(context = document);
    mountingPoint.mount(context = document.createElement('div'));
    mountingPoint.mount(context = document.querySelector('#some'));

    expect(count).toBe(3);
  });
});
