/* eslint-disable class-methods-use-this, no-param-reassign */

import partition from 'ramda/src/partition';
import T from 'ramda/src/T';
import EventEmitter from '@aspectus/event-emitter';

const toArray = iterable => Array.prototype.slice.call(iterable);

export function filesResolver(event) {
  let files = [];

  if (event.dataTransfer.files.length > 0) {
    files = toArray(event.dataTransfer.files);
  } else if (event.dataTransfer.items) {
    // FIXME
    // Method `.webkitGetAsEntry` is returning other type of file, so
    // it must be converted to a corresponding object type.
    files = toArray(event.dataTransfer.items)
      .map(item => item.webkitGetAsEntry());
  }

  return files;
}

export class Dropzone {
  constructor(config = {}) {
    /**
     * Dragging counter.
     *
     * @memberof DropZone
     */
    this.dragging = 0;

    /**
     * Responsible for DropZone event emitting.
     *
     * @memberof DropZone
     */
    this.emitter = new EventEmitter();

    /**
     * Elements, that are used as drop zones.
     *
     * @memberof DropZone
     */
    this.zones = [];

    /**
     * Input elements, to handle file selection.
     *
     * @memberof DropZone
     */
    this.inputs = [];

    this.config = {
      resolver: filesResolver,
      acceptor: T,
    };

    this.configure(config);

    this.onDragover = this.onDragover.bind(this);
    this.onDragenter = this.onDragenter.bind(this);
    this.onDragleave = this.onDragleave.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }

  configure(config = {}) {
    const resolver = config && config.resolver || this.config.resolver;
    const acceptor = config && config.acceptor || this.config.acceptor;

    this.config = { resolver, acceptor };

    this.resolver = resolver;
    this.acceptor = acceptor;
    this.partitioner = partition(this.acceptor);
  }

  /**
   * Binds DropZone to a particular elements.
   *
   * @param {*} zones Array of DOM elements that will be used as drop zones.
   * @param {*} inputs Array of DOM elements that will be used as file inputs.
   * @memberof DropZone
   */
  bind(zones, inputs) {
    this.zones = zones.concat(this.zones);
    this.inputs = inputs.concat(this.inputs);

    this.zones.forEach(element => {
      element.addEventListener('dragover', this.onDragover, false);
      element.addEventListener('dragenter', this.onDragenter, false);
      element.addEventListener('dragleave', this.onDragleave, false);
      element.addEventListener('drop', this.onDrop, false);
    });

    this.inputs.forEach(element => {
      element.addEventListener('change', this.onSelect);
    });
  }

  /**
   * Unbinds all listeners.
   *
   * @memberof DropZone
   */
  unbind() {
    this.zones.forEach(element => {
      element.removeEventListener('dragover', this.onDragover, false);
      element.removeEventListener('dragenter', this.onDragenter, false);
      element.removeEventListener('dragleave', this.onDragleave, false);
      element.removeEventListener('drop', this.onDrop, false);
    });

    this.inputs.forEach(element => {
      element.removeEventListener('change', this.onSelect);
    });

    delete this.emitter;
    delete this.zones;
    delete this.inputs;
  }

  /**
   * @param  {Event} event
   */
  onSelect(event) {
    // HTML file inputs do not seem to support folders, so assume this
    // is a flat file list.
    const files = toArray(event.target.files);
    const [accepted, rejected] = this.partitioner(files);

    this.emitter.emit('drop', { event, accepted, rejected });

    // Clearing out file input to be able to upload duplicates(for whatever reason).
    event.target.value = '';
  }

  onDragover(event) {
    event.stopPropagation();
    event.preventDefault();

    // Makes it possible to drag files from chrome's download bar
    // http://stackoverflow.com/questions/19526430/drag-and-drop-file-uploads-from-chrome-downloads-bar
    // Try is required to prevent bug in Internet Explorer 11 (SCRIPT65535 exception)
    let effect;
    try {
      effect = event.dataTransfer.effectAllowed;
    } catch (error) {} // eslint-disable-line no-empty

    event.dataTransfer.dropEffect = ((effect === 'move') || (effect === 'linkMove')) ? 'move' : 'copy';
  }

  /**
   * @param {Event} event Event to handle.
   */
  onDragenter(event) {
    event.stopPropagation();
    event.preventDefault();

    this.dragStart(event);
  }

  /**
   * @param {Event} event Event to handle.
   */
  onDragleave(event) {
    event.stopPropagation();
    event.preventDefault();

    this.dragStop(event);
  }

  dragStart(event) {
    if (this.dragging === 0) {
      this.emitter.emit('drag-start', { event });
    }

    this.dragging++;
  }

  dragStop(event, force = false) {
    this.dragging = force ? 0 : (this.dragging - 1);

    if (this.dragging === 0) {
      this.emitter.emit('drag-stop', { event });
    }
  }

  /**
   * @param {Event} event Drop event.
   */
  onDrop(event) {
    event.stopPropagation();
    event.preventDefault();

    const files = this.resolver(event);
    const [accepted, rejected] = this.partitioner(files);

    this.dragStop(event, true);
    this.emitter.emit('drop', { event, accepted, rejected });
  }
}
