# Element queries `eq` observer

Implementation of the element queries observer.

[Documentation](https://preusx.gitlab.io/aspectus/packages/eq.html)
