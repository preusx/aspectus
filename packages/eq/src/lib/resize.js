export const EQ_BLOCK = 'u-eq';
export const EQ_DEFAULT_PREFIX = `${EQ_BLOCK}--`;
export const EQ_DEFAULT_BREAKPOINTS = [
  'gte260', 'lt260',
  'gte390', 'lt390',
  'gte520', 'lt520',
  'gte780', 'lt780',
  'gte1040', 'lt1040',
];
export const EQ_BREAKPOINT_REGEX = /(.+?)([0-9]+)/i;

export const COMPARATORS = {
  '>': (a, b) => a > b,
  '>=': (a, b) => a >= b,
  '<': (a, b) => a < b,
  '<=': (a, b) => a <= b,
  '=': (a, b) => a == b, // eslint-disable-line eqeqeq
};
export const COMPARATOR_ALIASES = Object.assign(
  Object.keys(COMPARATORS).reduce((acc, x) => { acc[x] = x; return acc; }, {}),
  {
    gt: '>',
    gte: '>=',
    lt: '<',
    lte: '<=',
    is: '=',
  }
);
const defaultComparator = () => false;

export const resolveComparator = comparator => (
  COMPARATORS[COMPARATOR_ALIASES[comparator]] || defaultComparator
);
export const fit = (current, { comparator, value }) => (
  resolveComparator(comparator)(current, value)
);
export const fitMap = (current, comparators) => comparators.filter(x => fit(current, x));
export const diff = (from, to) => from.filter(f => !to.includes(f));
export const toggleClasses = (classes, el, apply = true) => classes.forEach(
  className => el.classList.toggle(className, apply)
);

export function parseBreakpoint(breakpoint) {
  const [key, comparator, value] = breakpoint.match(EQ_BREAKPOINT_REGEX);

  return { key, comparator, value: parseFloat(value) };
}

export class EqObserver {
  constructor({
    block = EQ_BLOCK,
    prefix = EQ_DEFAULT_PREFIX,
    breakpoints = EQ_DEFAULT_BREAKPOINTS,
  } = {}) {
    this.block = block;
    this.prefix = prefix;
    this.handleChange = this.handleChange.bind(this);
    this.getClassName = this.getClassName.bind(this);
    this.breakpoints = [];
    this.els = [];
    this.setTriggers(this.breakpoints);

    this.observer = new ResizeObserver(this.handleChange);

    this.update({ breakpoints });
  }

  setTriggers(breakpoints) {
    this.triggers = breakpoints.map(parseBreakpoint);
  }

  handleChange(entries) {
    entries.forEach(entry => {
      const { width } = entry.contentRect;
      this.apply(this.triggers, fitMap(width, this.triggers), entry.target);
    });
  }

  getClassName({ key }) {
    return `${this.prefix}${key}`;
  }

  apply(base, applicable, el) {
    const setClasses = applicable.map(this.getClassName);
    const removeClasses = diff(base.map(this.getClassName), setClasses);

    toggleClasses(removeClasses, el, false);
    toggleClasses(setClasses, el, true);
  }

  update({
    breakpoints = this.breakpoints,
  } = {}) {
    const oldTriggers = this.triggers;

    this.breakpoints = breakpoints;
    this.setTriggers(this.breakpoints);
    this.els.forEach(el => {
      this.apply(
        oldTriggers,
        fitMap(el.getBoundingClientRect().width, this.triggers),
        el
      );
    });
  }

  bind(el) {
    this.els.push(el);
    this.observer.observe(el);
    this.update();

    return this;
  }

  unbind(el) {
    const index = this.els.indexOf(el);
    if (index) {
      this.els.splice(index, 1);
    }
    this.observer.unobserve(el);

    return this;
  }

  clear() {
    this.observer.disconnect();
    this.els = [];

    return this;
  }
}
