import { fitMap, parseBreakpoint } from '@/main';

describe('Breakpoint parsing', () => {
  it('Simple', () => {
    expect(parseBreakpoint('>=50')).toHaveProperty('comparator', '>=');
    expect(parseBreakpoint('>=50')).toHaveProperty('value', 50);

    expect(parseBreakpoint('>50')).toHaveProperty('comparator', '>');
    expect(parseBreakpoint('>50')).toHaveProperty('value', 50);

    expect(parseBreakpoint('<50000000')).toHaveProperty('comparator', '<');
    expect(parseBreakpoint('<50000000')).toHaveProperty('value', 50000000);
  });

  it('Aliases', () => {
    expect(parseBreakpoint('gte50')).toHaveProperty('comparator', 'gte');
    expect(parseBreakpoint('gte50')).toHaveProperty('value', 50);

    expect(parseBreakpoint('gt50')).toHaveProperty('comparator', 'gt');
    expect(parseBreakpoint('gt50')).toHaveProperty('value', 50);

    expect(parseBreakpoint('lt50000000')).toHaveProperty('comparator', 'lt');
    expect(parseBreakpoint('lt50000000')).toHaveProperty('value', 50000000);
  });
});

describe('Sizes fit', () => {
  it('Simple', () => {
    const breakpoints = ['>=50', '<10', '>=100', '>100'].map(parseBreakpoint);

    expect(fitMap(100, breakpoints)).toHaveLength(2);
    expect(fitMap(50, breakpoints)).toHaveLength(1);
    expect(fitMap(5, breakpoints)).toHaveLength(1);
  });

  it('Aliases', () => {
    const breakpoints = ['gte50', 'lt10', 'gte100', 'gt100'].map(parseBreakpoint);

    expect(fitMap(100, breakpoints)).toHaveLength(2);
    expect(fitMap(50, breakpoints)).toHaveLength(1);
    expect(fitMap(5, breakpoints)).toHaveLength(1);
  });
});
