import { createProxy } from '@aspectus/promise-proxy';

export default createProxy({
  progress(callback) {
    if (this.$data.progressEmitter && this.$data.progressEmitter.on) {
      return this.$data.progressEmitter.on('progress', callback);
    }

    return this;
  },
});
