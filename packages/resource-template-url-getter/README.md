# Template url getter

Template url getter function for `@aspectus/resource`.

[Documentation](https://preusx.gitlab.io/aspectus/packages/resource-template-url-getter.html)
