import urlTemplate from 'url-template';

export default function makeTemplateGetter(template) {
  const compiled = urlTemplate.parse(template);

  return compiled.expand.bind(compiled);
}
