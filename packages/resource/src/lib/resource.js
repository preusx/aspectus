import { sameResult } from './utils';

export function createResource({
  urlGetter,
  middleware = sameResult,
  config = {},
  headers = {},
  fetcher = null,
} = {}) {
  return (parameters = {}, body = null) => {
    const caller = fetcher !== null ? fetcher : fetch;
    const prepared = middleware({ parameters, body, headers, config });

    return caller(urlGetter(prepared.parameters), {
      ...prepared.config,
      headers: prepared.headers,
      body: prepared.body,
    });
  };
}

export default createResource;
