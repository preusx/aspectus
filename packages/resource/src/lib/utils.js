/* eslint-disable import/prefer-default-export */

export const sameResult = x => x;

export function isFunction(value) {
  return (
    Object.prototype.toString.call(value) === '[object Function]' ||
    typeof value === 'function'
  );
}
