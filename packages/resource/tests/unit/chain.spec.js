import fetchMock from 'fetch-mock';

import { withFetchMock } from './utils';

import {
  ResourceChain, configMiddleware, defaultsMiddleware, overrideMiddleware,
} from '@/main';

describe('Resource creation chain', () => {
  it('Parameters change', () => {
    const value = 'value';
    const getter = () => value;
    const chain = new ResourceChain()
      .config({ key: value })
      .headers('key', value)
      .url(getter)
      .middleware(configMiddleware(defaultsMiddleware({ key: 'other' })));

    expect(chain.parameters).toHaveProperty('config.key', value);
    expect(chain.parameters).toHaveProperty('headers.key');
    expect(chain.parameters).toHaveProperty('middleware');
    expect(chain.parameters).toHaveProperty('urlGetter', getter);
  });

  it('Execution', () => {
    const type = 'application/json';
    const getter = () => 'http://google.com/';
    const chain = new ResourceChain()
      .config({
        method: 'GET',
        credentials: 'omit',
      })
      .headers('Content-Type', type)
      .url(getter)
      .middleware(configMiddleware(defaultsMiddleware({ credentials: 'same-origin' })))
      .middleware(configMiddleware(overrideMiddleware({ method: 'POST' })));

    withFetchMock(m => {
      m.mock((url, options) => {
        expect(url).toBe(getter());
        expect(options).toHaveProperty('method', 'POST');
        expect(options).toHaveProperty('credentials', 'omit');
        expect(options).toHaveProperty('headers.Content-Type', type);

        return true;
      }, 200);

      chain.execute({}, {});

      expect(m.called(getter())).toBe(true);
    });
  });

  it('Middleware execution order check', () => {
    const getter = () => 'http://google.com/';
    const chain = new ResourceChain()
      .url(getter)
      .middleware(configMiddleware(overrideMiddleware({ method: 'POST' })), 600)
      .middleware(configMiddleware(overrideMiddleware({ method: 'GET' })), 400);

    withFetchMock(m => {
      m.mock((url, options) => {
        expect(url).toBe(getter());
        expect(options).toHaveProperty('method', 'POST');

        return true;
      }, 200);

      chain.execute({}, {});

      expect(m.called(getter())).toBe(true);
    });

    const chain2 = chain
      .middleware(configMiddleware(overrideMiddleware({ method: 'GET' })), 601);

    withFetchMock(m => {
      m.mock((url, options) => {
        expect(url).toBe(getter());
        expect(options).toHaveProperty('method', 'GET');

        return true;
      }, 200);

      chain2.execute({}, {});

      expect(m.called(getter())).toBe(true);
    });

    const chain3 = chain2
      .middleware(configMiddleware(overrideMiddleware({ method: 'POST' })), 601);

    withFetchMock(m => {
      m.mock((url, options) => {
        expect(url).toBe(getter());
        expect(options).toHaveProperty('method', 'POST');

        return true;
      }, 200);

      chain3.execute({}, {});

      expect(m.called(getter())).toBe(true);
    });
  });

  it('Custom fetcher', async () => {
    const getter = () => '';
    const chain = new ResourceChain()
      .config({ method: 'GET' })
      .url(getter);

    const m = fetchMock.sandbox();
    m.mock('*', 200);

    await expect(chain.execute({}, {})).rejects.toThrow();
    expect(m.called(getter())).toBe(false);

    chain.fetcher(m).execute({}, {});
    expect(m.called(getter())).toBe(true);
    await expect(chain.execute({}, {})).rejects.toThrow();
  });

  it('Url transforming', async () => {
    const getter = p => `/?filter${encodeURI(JSON.stringify(p))}`;
    const chain = new ResourceChain()
      .config({ method: 'GET' })
      .url(getter);
    const parameters = { first: 'data' };

    withFetchMock(m => {
      m.mock((url, options) => {
        expect(url).toBe(getter(parameters));
        expect(options).toHaveProperty('method', 'GET');

        return true;
      }, 200);

      chain.execute(parameters, {});

      expect(m.called(getter(parameters))).toBe(true);
    });
  });

  it('Custom result transformers', async () => {
    const getter = () => '';
    const fetcher = (url, { body }) => Promise.resolve(body);
    const chain = new ResourceChain()
      .config({ method: 'GET' })
      .fetcher(fetcher)
      .url(getter)
      .transform(result => Promise.resolve(result.slice(0, 2)))
      .transform(result => Promise.resolve(result.reverse()));

    const content = await chain.execute({}, [
      'first', 'second', 'third', 'fourth',
    ]);
    expect(content).toHaveLength(2);
    expect(content).toContain('first');
    expect(content).toContain('second');
    expect(content[0]).toBe('second');
  });
});
