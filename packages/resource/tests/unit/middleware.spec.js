import './utils';

import {
  keyMiddleware,
  configMiddleware,
  parametersMiddleware,
  headersMiddleware,
  bodyMiddleware,
  defaultsMiddleware,
  overrideMiddleware,
  jsonRequestMiddleware,
} from '@/main';

describe('Key middleware helper', () => {
  it('Transforms provided key from config', () => {
    const value = 'value';

    expect(keyMiddleware('key', () => value, {})).toHaveProperty('key', value);
  });

  it('Currying check', () => {
    const value = 'value';
    const someChanger = keyMiddleware('some');
    const keyChanger = keyMiddleware('key', () => value);
    const valueChanger = keyMiddleware('value')(() => value);

    expect(someChanger(() => value, {})).toHaveProperty('some', value);
    expect(someChanger(() => value)({})).toHaveProperty('some', value);
    expect(keyChanger({})).toHaveProperty('key', value);
    expect(valueChanger({})).toHaveProperty('value', value);

    const bothKeyValue = keyChanger(valueChanger({}));
    expect(bothKeyValue).toHaveProperty('key', value);
    expect(bothKeyValue).toHaveProperty('value', value);
  });
});

describe('Object merger middleware', () => {
  it('`config` key middleware', () => {
    const value = 'value';

    expect(configMiddleware(keyMiddleware('key', () => value))({}))
      .toHaveProperty('config.key', value);
  });

  it('`parameters` key middleware', () => {
    const value = 'value';

    expect(parametersMiddleware(keyMiddleware('key', () => value))({}))
      .toHaveProperty('parameters.key', value);
  });

  it('`headers` key middleware', () => {
    const value = 'value';

    expect(headersMiddleware(keyMiddleware('key', () => value))({}))
      .toHaveProperty('headers.key', value);
  });

  it('`body` key middleware', () => {
    const value = 'value';

    expect(bodyMiddleware(keyMiddleware('key', () => value))({}))
      .toHaveProperty('body.key', value);
  });
});

describe('Configuration middleware helpers', () => {
  it('Defaults setter', () => {
    const value = 'value';
    const initial = 'initial';
    const defaultSetter = defaultsMiddleware({ key: value });

    expect(defaultSetter({})).toHaveProperty('key', value);
    expect(defaultSetter({ key: initial })).toHaveProperty('key', initial);
  });

  it('Overrides setter', () => {
    const value = 'value';
    const initial = 'initial';
    const defaultSetter = overrideMiddleware({ key: value });

    expect(defaultSetter({})).toHaveProperty('key', value);
    expect(defaultSetter({ key: initial })).toHaveProperty('key', value);
  });

  it('JSON request', () => {
    expect(jsonRequestMiddleware({})).toHaveProperty('headers.Content-Type', 'application/json');
    expect(jsonRequestMiddleware({ body: { key: 'value' } })).toHaveProperty('body', '{"key":"value"}');
  });
});
