const fetchMock = require('fetch-mock');
const nodeFetch = require('node-fetch');

global.fetch = nodeFetch;

function withFetchMock(caller) {
  const f = fetchMock.sandbox();
  f.reset();
  f.resetBehavior();

  global.fetch = f;
  caller(f);

  f.reset();
  f.resetBehavior();
  global.fetch = nodeFetch;
}

module.exports = {
  withFetchMock,
};
