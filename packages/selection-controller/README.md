# Selection controller

Controller for multiple/singular options selection.

[Documentation](https://preusx.gitlab.io/aspectus/packages/selection-controller.html)
