import { prepareOptions, prepareValue, prepareData, updateValue, resultValue } from '@/main';

describe('Option preparation', () => {
  const options = [
    { id: null, label: 'Not selected' },
    { id: 'other-option-22340912094123340395710', label: 'Other option' },
    { id: 'third-option-09872082502459786238384', label: 'Third option' },
  ];

  it('Not selected', () => {
    const [first, second, third] = prepareOptions(options, null, x => x.id);

    expect(first.selected).toBeTruthy();
    expect(second.selected).not.toBeTruthy();
    expect(third.selected).not.toBeTruthy();

    expect(second.key).toBe(options[1].id);
    expect(second.value).toBe(options[1]);
  });

  it('Selected', () => {
    const [first, second, third] = prepareOptions(options, [options[1]], x => x.id);

    expect(first.selected).not.toBeTruthy();
    expect(second.selected).toBeTruthy();
    expect(third.selected).not.toBeTruthy();
  });

  it('Multiple selections', () => {
    const [first, second, third] = prepareOptions(
      options, [options[1].id, options[2].id], x => x.id
    );

    expect(first.selected).not.toBeTruthy();
    expect(second.selected).toBeTruthy();
    expect(third.selected).toBeTruthy();

    const [first2, second2, third2] = prepareOptions(
      options, [options[0], options[2].id], x => x.id
    );

    expect(first2.selected).toBeTruthy();
    expect(second2.selected).not.toBeTruthy();
    expect(third2.selected).toBeTruthy();
  });
});

describe('Value preparation', () => {
  const options = [
    { id: null, label: 'Not selected' },
    { id: 'other-option-22340912094123340395710', label: 'Other option' },
    { id: 'third-option-09872082502459786238384', label: 'Third option' },
  ];
  const prepared = prepareOptions(options, null, x => x.id);

  it('Empty option', () => {
    const value = prepareValue(prepared, null, x => x.id);

    expect(value).toHaveLength(1);
    expect(value[0]).toBe(options[0]);
  });

  it('Selected option', () => {
    const value = prepareValue(prepared, [options[1]], x => x.id);

    expect(value).toHaveLength(1);
    expect(value[0]).toBe(options[1]);
  });

  it('Multiple option', () => {
    const value = prepareValue(prepared, [options[1], options[2].id], x => x.id);

    expect(value).toHaveLength(2);
    expect(value[0]).toBe(options[1]);
    expect(value[1]).toBe(options[2]);
  });
});

describe('Data preparation', () => {
  const data = [
    { id: null, label: 'Not selected' },
    { id: 'other-option-22340912094123340395710', label: 'Other option' },
    { id: 'third-option-09872082502459786238384', label: 'Third option' },
  ];

  it('Empty option', () => {
    const { value, options: [first, second, third] } = prepareData(data, null, x => x.id);

    expect(value).toHaveLength(1);
    expect(value[0]).toBe(data[0]);

    expect(first.selected).toBeTruthy();
    expect(second.selected).not.toBeTruthy();
    expect(third.selected).not.toBeTruthy();
  });

  it('Selected option', () => {
    const { value, options: [first, second, third] } = prepareData(data, data[1].id, x => x.id);

    expect(value).toHaveLength(1);
    expect(value[0]).toBe(data[1]);

    expect(first.selected).not.toBeTruthy();
    expect(second.selected).toBeTruthy();
    expect(third.selected).not.toBeTruthy();
  });

  it('Multiple option', () => {
    const { value, options: [first, second, third] } = prepareData(
      data, [data[1], data[2].id], x => x.id
    );

    expect(value).toHaveLength(2);
    expect(value[0]).toBe(data[1]);
    expect(value[1]).toBe(data[2]);

    expect(first.selected).not.toBeTruthy();
    expect(second.selected).toBeTruthy();
    expect(third.selected).toBeTruthy();
  });

  it('No key getter', () => {
    const { value, options: [empty, first, second, third] } = prepareData( // eslint-disable-line no-unused-vars, max-len
      [null, 1, 2, 3], [2, 3]
    );

    expect(value).toHaveLength(2);
    expect(value[0]).toBe(2);
    expect(value[1]).toBe(3);

    expect(first.selected).not.toBeTruthy();
    expect(second.selected).toBeTruthy();
    expect(third.selected).toBeTruthy();
  });

  it('Non existing option in value', () => {
    const { value, options: [empty, first, second, third] } = prepareData( // eslint-disable-line no-unused-vars, max-len
      [null, 1, 2, 3], [2, 3, 4]
    );

    expect(value).toHaveLength(3);
    expect(value[0]).toBe(2);
    expect(value[1]).toBe(3);
    expect(value[2]).toBe(4);

    expect(first.selected).not.toBeTruthy();
    expect(second.selected).toBeTruthy();
    expect(third.selected).toBeTruthy();
  });

  it('Falsy values selection', () => {
    const { value, options: [empty, first, second, third] } = prepareData( // eslint-disable-line no-unused-vars, max-len
      [null, true, false, Infinity], [true, NaN, Infinity]
    );

    expect(value).toHaveLength(3);
    expect(value[0]).toBe(true);
    expect(value[1]).toBe(NaN);
    expect(value[2]).toBe(Infinity);

    expect(first.selected).toBeTruthy();
    expect(second.selected).not.toBeTruthy();
    expect(third.selected).toBeTruthy();
  });
});

describe('Value update', () => {
  const data = [
    { id: null, label: 'Not selected' },
    { id: 'other-option-22340912094123340395710', label: 'Other option' },
    { id: 'third-option-09872082502459786238384', label: 'Third option' },
  ];

  it('Singular select', () => {
    const updated = updateValue(data[1], [null], x => x.id);

    expect(updated).toHaveLength(1);
    expect(updated[0]).toBe(data[1]);

    const updated2 = updateValue(data[1], [data[2]], x => x.id);

    expect(updated2).toHaveLength(1);
    expect(updated2[0]).toBe(data[1]);
  });

  it('Multiple select', () => {
    const updated = updateValue(data[1], [null], x => x.id, true);

    expect(updated).toHaveLength(1);
    expect(updated[0]).toBe(data[1]);

    const updated1 = updateValue(data[1], [data[2], data[1]], x => x.id, true);

    expect(updated1).toHaveLength(1);
    expect(updated1[0]).toBe(data[2]);

    const updated2 = updateValue(data[1], [data[2]], x => x.id, true);

    expect(updated2).toHaveLength(2);
    expect(updated2[0]).toBe(data[2]);
    expect(updated2[1]).toBe(data[1]);

    const updated3 = updateValue(data[1], [data[1]], x => x.id, true);

    expect(updated3).toHaveLength(1);
    expect(updated3[0]).toBe(null);
  });

  it('Set null select', () => {
    const updated = updateValue(null, [data[1]], x => x.id);

    expect(updated).toHaveLength(1);
    expect(updated[0]).toBe(null);

    const updated2 = updateValue(null, [data[1], data[2]], x => x.id, true);

    expect(updated2).toHaveLength(1);
    expect(updated2[0]).toBe(null);

    const updated3 = updateValue(data[0], [data[1], data[2]], x => x.id, true);

    expect(updated3).toHaveLength(1);
    expect(updated3[0]).toBe(null);

    const updated4 = updateValue([data[1]], [data[1], data[2]], x => x.id, true);

    expect(updated4).toHaveLength(1);
    expect(updated4[0]).toBe(data[1]);

    const updated5 = updateValue([], [data[1], data[2]], x => x.id, true);

    expect(updated5).toHaveLength(1);
    expect(updated5[0]).toBe(null);
  });
});

describe('Result value transform', () => {
  it('Singular selection', () => {
    expect(resultValue([null])).toBe(null);
    expect(resultValue([3])).toBe(3);
    expect(resultValue([4, 3])).toBe(4);
  });

  it('Multiple selection', () => {
    expect(resultValue([null], true)).toStrictEqual([null]);
    expect(resultValue([3], true)).toStrictEqual([3]);
    expect(resultValue([4, 3], true)).toStrictEqual([4, 3]);
  });
});
