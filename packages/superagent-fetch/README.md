# Fetch API adapter for superagent usage

Custom fetcher that adapts [superagent](https://github.com/visionmedia/superagent) library to the default Fetch API.

[Documentation](https://preusx.gitlab.io/aspectus/packages/superagent-fetch.html)
