# Vee control descriptor

Control descriptor integrated with vee validate provider. Special wrapper for control element that adds additional description.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vee-control-descriptor.html)
