/* eslint-disable object-property-newline */

import { mergeContext } from '@aspectus/vue-utils';
import { ControlDescriptor } from '@aspectus/vue-control-descriptor';
import { ValidationProvider } from 'vee-validate';
import Controller from './InteractionHideErrorsController';
import Gate from './VeeValidateInputHandlerGate';

export default {
  name: 'vee-control-descriptor',
  functional: true,
  props: {
    descriptorComponent: { default: () => ControlDescriptor },
    providerComponent: { default: () => ValidationProvider },
    controllerComponent: { default: () => Controller },
    disabled: Boolean,
    required: Boolean,
    readonly: Boolean,
    bails: Boolean,
    immediate: Boolean,
    debounce: Number,
    mode: {},
    customMessages: {},
    name: String,
    vid: {},
    value: {},
    rules: {},
  },
  render(h, context) {
    const {
      descriptorComponent, providerComponent, controllerComponent,
      disabled, required, readonly,
      bails, immediate, debounce, mode, customMessages,
      name, vid, value, rules,
    } = context.props;

    return h(
      providerComponent, {
        props: {
          disabled, required, readonly,
          bails, immediate, debounce, mode, customMessages,
          name, vid, value,
          skipIfEmpty: !required,
          rules: readonly ? '' : rules,
          slim: true,
        },
        scopedSlots: {
          default(props) {
            const data = mergeContext(context.data, {
              attrs: {
                disabled, readonly,
                name, vid, value, rules,
                required: required || props.required,
                errors: props.errors,
                'provider-context': props,
                'descriptor-component': descriptorComponent,
                'gated-handler-component': controllerComponent,
              },
            });

            // FIXME: This gate mechanics unfortunately does not fix the
            // constant handlers amount increase issue.
            return h(Gate, data, context.children);
          },
        },
      }
    );
  },
};
