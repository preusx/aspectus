/* eslint-disable prefer-object-spread */

import { mergeContext, makeF } from '@aspectus/vue-utils';
import { InteractionStateController } from '@aspectus/vue-control-interaction-state-controller';

export default makeF(
  (h, context) => {
    const { descriptorComponent, providerContext, errors, value } = context.props;
    const { input, focus, blur, ...listeners } = context.data.on;
    const baseHandlers = { input, focus, blur };

    // Problems with undefined vue event handlers? Remove them.
    Object.keys(baseHandlers).forEach(key => {
      if (typeof baseHandlers[key] === 'undefined') {
        delete baseHandlers[key];
      }
    });

    return h(
      InteractionStateController,
      {
        // HACK: Tricky hack for vee-validate provider v-model-detection
        attrs: { value },
        on: baseHandlers,
        scopedSlots: {
          default: ({ changing, interacted, interacting, handlers }) => {
            const definition = mergeContext(context.data, {
              attrs: {
                value,
                'provider-context': providerContext,
                errors: (
                  !providerContext.pending &&
                  (!interacting || (interacting && !changing)) &&
                  (interacted || providerContext.touched)
                )
                  ? errors
                  : [],
              },
            });
            definition.on = Object.assign({}, handlers, listeners);

            return h(descriptorComponent, definition, context.children);
          },
        },
      },
      null
    );
  },
  {
    descriptorComponent: {},
    providerContext: {},
    errors: {},
    value: {},
  },
  'vee-control-descriptor-controller-interaction-hide-errors'
);
