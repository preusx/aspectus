import Descriptor from './Descriptor';
import NoneController from './NoneController';
import InteractionHideErrorsController from './InteractionHideErrorsController';

export function install(Vue, {
  name = Descriptor.name,
  noneControllerName = NoneController.name,
  interactionHideErrorsControllerName = InteractionHideErrorsController.name,
} = {}) {
  Vue.component(name, Descriptor);
  Vue.component(noneControllerName, NoneController);
  Vue.component(interactionHideErrorsControllerName, InteractionHideErrorsController);
}

export { Descriptor, NoneController, InteractionHideErrorsController };
export default { install };
