# vee-i18n
- `trans` - переводит все, на сегодняшний день, встроенные правила  пакета `vee-validate`. И теперь они будут доступны в `rosetta`.

## Установка 

```bash
yarn add @aspectus/vee-i18n
```
```js
import trans from '@aspectus/vee-i18n'
```

## Использование

```js 
import Vue from 'vue'
import VueI18n from 'vue-i18n'

//Ипортируем нужные правила из vee-validate
import {
  required, 
  min, 
  max,
  email,
} from 'vee-validate/dist/rules'

const rules = {
  required,
  min,
  max,
  email,
}
//или
import { 
  * as rules 
} from 'vee-validate/dist/rules'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  ...some_params,
})
// вызываем, передав аргументом инстанс i18n
trans(i18n, rules)
```

Вот и все) Все [переводы появились](http://joxi.ru/Rmz0O7lHRoM6ZA).