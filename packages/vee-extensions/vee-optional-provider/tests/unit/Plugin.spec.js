import { createLocalVue } from '@vue/test-utils';
import OptionalProvider from '@/OptionalProvider';
import Plugin from '@/main';

describe('Plugin', () => {
  it('Installs silently', () => {
    const localVue = createLocalVue();

    expect(() => localVue.use(Plugin)).not.toThrow();
  });

  it('Installs OptionalProvider', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin);

    expect(localVue.options.components[OptionalProvider.name]).toBeDefined();
  });

  it('Installs custom named OptionalProvider', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { name: 'v-optional-provider' });

    expect(localVue.options.components[OptionalProvider.name]).not.toBeDefined();
    expect(localVue.options.components['v-optional-provider']).toBeDefined();
  });
});
