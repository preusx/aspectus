/* eslint-disable prefer-object-spread, import/prefer-default-export */

import {
  StylingPropsMixin,
  extractProps,
  constructProps,
  BLOCK_MODIFIERS,
  BLOCK_STATES,
  MODIFIER_PROP_DEFINITION,
  STATE_PROP_DEFINITION,
} from '@aspectus/vue-bem-styling';
import { mergeContext, makeF } from '@aspectus/vue-utils';

export function prepareResolver(options = {}) {
  const { modifiers = {}, states = {} } = options;

  const allModifiers = Object.assign({}, BLOCK_MODIFIERS, modifiers);
  const allStates = Object.assign({}, BLOCK_STATES, states);
  const modifiersExtractor = extractProps(allModifiers);
  const statesExtractor = extractProps(allStates);

  return props => [
    modifiersExtractor(props), statesExtractor(props),
  ];
}

export function prepareProps({ modifiers = {}, states = {}, props = {} } = {}) {
  return Object.assign(
    {},
    constructProps(modifiers, MODIFIER_PROP_DEFINITION),
    constructProps(states, STATE_PROP_DEFINITION),
    props
  );
}

export function generator(options = {}) {
  const {
    tag = 'div',
    block,
    state,
    transform = null,
    children = null,
  } = options;
  const resolver = prepareResolver(options);

  return makeF(
    (h, context) => {
      const props = transform ? transform(context.props) : context.props;
      const [currentModifiers, currentStates] = resolver(props);

      return h(
        tag,
        mergeContext(context.data, {
          class: [block(currentModifiers), state ? state(currentStates) : null],
        }),
        children ? children(h, context, options) : context.children
      );
    },
    prepareProps(options),
    undefined,
    { mixins: [StylingPropsMixin] }
  );
}
