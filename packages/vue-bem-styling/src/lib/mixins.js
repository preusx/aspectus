/* eslint-disable prefer-object-spread */

import { createBlockGenerator, createStateGenerator } from '@aspectus/bem';
import { BLOCK_MODIFIERS, BLOCK_STATES } from './const';
import { extractProps } from './utils';

export const MODIFIER_PROP_DEFINITION = {
  type: [String, Array, Boolean, Number],
};

export const STATE_PROP_DEFINITION = {
  type: Boolean,
  default: false,
};

const samePropReducer = prop => (acc, x) => {
  acc[x] = prop;
  return acc;
};

export const constructProps = (map, definition) => Object.keys(map)
  .reduce(samePropReducer(definition), {});

export const MODIFIER_PROPS = constructProps(
  BLOCK_MODIFIERS, MODIFIER_PROP_DEFINITION
);
export const STATE_PROPS = constructProps(
  BLOCK_STATES, STATE_PROP_DEFINITION
);

export const STYLING_PROPS = Object.assign({}, MODIFIER_PROPS, STATE_PROPS);

export const StylingPropsMixin = {
  props: STYLING_PROPS,
};

export const createStylingFunctionalMixin = ({
  blockConfig = {},
  stateConfig = {},
  modifiers = BLOCK_MODIFIERS,
  states = BLOCK_STATES,
} = {}) => ({
  mixins: [StylingPropsMixin],
  props: {
    blockName: String,
    // Block class generator
    bcg: { type: Function, default: createBlockGenerator(blockConfig) },
    // Block state generator
    bsg: { type: Function, default: createStateGenerator(stateConfig) },
    bclass: {
      type: Function,
      default(props = this) {
        return [
          props.bcg(props.blockName)(extractProps(modifiers)(props)),
          props.bsg(extractProps(states)(props)),
        ];
      },
    },
    eclass: {
      type: Function,
      default(element, md = {}, props = this) {
        return props.bcg(props.blockName)(element, md);
      },
    },
  },
});

export const StylingFunctionalMixin = createStylingFunctionalMixin();

export const StylingMixin = {
  mixins: [StylingPropsMixin],

  BLOCK_MODIFIERS,
  BLOCK_STATES,

  computed: {
    modifiers() { return this.extractModifiers(); },
    states() { return this.extractStates(); },
  },

  methods: {
    extractModifiers(props = this) {
      return extractProps(this.$options.BLOCK_MODIFIERS)(props);
    },
    extractStates(props = this) {
      return extractProps(this.$options.BLOCK_STATES)(props);
    },
  },
};
