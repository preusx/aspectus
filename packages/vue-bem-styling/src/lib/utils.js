import curry from 'ramda/src/curry';
import { BLOCK_MODIFIERS, BLOCK_STATES } from './const';

export const mapSame = (...keys) => keys.reduce((acc, x) => {
  acc[x] = x;
  return acc;
}, {});
export const extractProps = curry(
  (propsMap, data) => Object.keys(propsMap)
    .reduce((acc, x) => {
      acc[propsMap[x]] = data[x];
      return acc;
    }, {})
);
export const extractChangers = (
  props, modifiers = BLOCK_MODIFIERS, states = BLOCK_STATES
) => [extractProps(modifiers, props), extractProps(states, props)];
