import { extractProps, BLOCK_MODIFIERS, BLOCK_STATES } from '@/main';

describe('Styling utilities', () => {
  it('Modifiers extractor', () => {
    expect(extractProps(BLOCK_MODIFIERS, {
      theme: 'themeName',
      view: 'some',
      styling: 'different',
      variant: 'opposite',
      appearance: 'default',
      kind: 'foreign',

      align: 'to-left',
      fit: 'container',

      other: 'not-to-be',
    })).toStrictEqual({
      theme: 'themeName',
      view: 'some',
      styling: 'different',
      variant: 'opposite',
      appearance: 'default',
      kind: 'foreign',

      align: 'to-left',
      fit: 'container',
    });
  });

  it('States extractor', () => {
    expect(extractProps(BLOCK_STATES, {
      disabled: 'disabled',
      passive: 'passive',
      readonly: 'readonly',
      required: 'required',
      invalid: 'invalid',
      active: 'active',
      opened: 'opened',
      hidden: 'hidden',

      other: 'not-to-be',
    })).toStrictEqual({
      disabled: 'disabled',
      passive: 'passive',
      readonly: 'readonly',
      required: 'required',
      invalid: 'invalid',
      active: 'active',
      opened: 'opened',
      hidden: 'hidden',
    });
  });
});
