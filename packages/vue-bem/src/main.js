import {
  Directive,
  BlockDirective, BlockDirectiveUnbindable,
  StateDirective, StateDirectiveUnbindable,
} from './lib';

export function install(Vue, {
  blockNameKey = 'name',
  block = 'bem',
  state = 'state',
  naming = {},
  unbindable = false,
} = {}) {
  Vue.directive(block, new (
    unbindable ? BlockDirectiveUnbindable : BlockDirective
  )({ blockNameKey, ...naming }));
  Vue.directive(state, new (
    unbindable ? StateDirectiveUnbindable : StateDirective
  )(naming));
}

export {
  Directive,
  BlockDirective, BlockDirectiveUnbindable,
  StateDirective, StateDirectiveUnbindable,
};
export default { install };
