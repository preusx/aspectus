import { createLocalVue, mount } from '@vue/test-utils';
import Plugin from '@/main';

import BemBlock from './fixtures/BemBlock.vue';
import NestedBemBlock from './fixtures/NestedBemBlock.vue';
import StateBlock from './fixtures/StateBlock.vue';

describe('Directives', () => {
  it('Bem directive', () => {
    const localVue = createLocalVue();
    localVue.use(Plugin);

    const wrapper = mount(BemBlock, { localVue });

    expect(wrapper.is('.block')).toBeTruthy();
    expect(wrapper.contains('.block__element')).toBeTruthy();
    expect(wrapper.contains('.block__second.block__second--modifier')).toBeTruthy();
    expect(wrapper.contains('.block__second.block__second--other_second')).toBeTruthy();
  });

  it('State directive', () => {
    const localVue = createLocalVue();
    localVue.use(Plugin);

    const wrapper = mount(StateBlock, { localVue });

    expect(wrapper.is('.block.is-hidden.is-visible')).toBeTruthy();
  });

  it('Nested bem directive', () => {
    const localVue = createLocalVue();
    localVue.use(Plugin);

    const wrapper = mount(NestedBemBlock, { localVue });

    expect(wrapper.is('.block.nested-bem-block')).toBeTruthy();
  });
});
