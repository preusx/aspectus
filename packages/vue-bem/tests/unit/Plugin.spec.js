import { createLocalVue } from '@vue/test-utils';
import Plugin from '@/main';

describe('Plugin', () => {
  it('Installs silently', () => {
    const localVue = createLocalVue();

    expect(() => localVue.use(Plugin)).not.toThrow();
  });

  it('Installs directives', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin);

    expect(localVue.options.directives.bem).toBeDefined();
    expect(localVue.options.directives.state).toBeDefined();
  });

  it('Installs custom named directives', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { block: 'bm', state: 'is' });

    expect(localVue.options.directives.bm).toBeDefined();
    expect(localVue.options.directives.is).toBeDefined();
  });

  it('Installs custom config', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { naming: { n: 'b-' } });

    expect(localVue.options.directives.bem).toHaveProperty('config.n', 'b-');
  });
});
