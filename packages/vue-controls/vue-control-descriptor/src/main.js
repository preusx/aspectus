import { ControlDescriptor, ControlDescriptorLayout } from './lib';

export function install(Vue, {
  name = ControlDescriptor.name,
  layoutName = ControlDescriptorLayout.name,
} = {}) {
  Vue.component(name, ControlDescriptor);
  Vue.component(layoutName, ControlDescriptorLayout);
}

export { ControlDescriptor, ControlDescriptorLayout };
export default {
  ControlDescriptor, ControlDescriptorLayout, install,
};
