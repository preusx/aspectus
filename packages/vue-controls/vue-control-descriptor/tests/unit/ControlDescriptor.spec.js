import { createLocalVue } from '@vue/test-utils';
import Plugin from '@/main';

const localVue = createLocalVue();
const usePlugin = () => localVue.use(Plugin);

describe('Plugin', () => {
  it('Installs silently', () => {
    expect(usePlugin).not.toThrow();
  });
});
