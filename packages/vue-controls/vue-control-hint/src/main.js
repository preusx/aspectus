import Hint from './Hint';

export function install(Vue, { name = Hint.name } = {}) {
  Vue.component(name, Hint);
}

export { Hint };
export default { install };
