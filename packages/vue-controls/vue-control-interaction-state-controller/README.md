# Vue control interaction state controller

Component for handling state of user's interaction with some control element.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-control-interaction-state-controller.html)
