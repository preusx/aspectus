import { createBlockGenerator } from '@aspectus/bem';

export const b = createBlockGenerator({ n: 'ds-' });
export const t = createBlockGenerator({ n: 't-' });
export const u = createBlockGenerator({ n: 'u-' });
export const ud = createBlockGenerator({ n: 'u-', v: '-' });
