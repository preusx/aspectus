import { mapSame } from './utils';

export const SIZE = mapSame('size');
export const SPACE = mapSame('space');
export const IN_OF = mapSame('indent', 'offset');
