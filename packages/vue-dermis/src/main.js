import {
  Caption,
  Inliner,
  Link,
  Avatar,
  AspectRatio,
  AspectRatioBody,
  Panel,
  PanelElement,
  Section,
  SectionElement,
  Table,
  TableCaption,
  TableRow,
  TableCell,
  Content,
  Words,
  Display,
  SpreadedInteractor,
} from './components';

export function install(Vue, {
  prefix = 'ds-',
  prefixType = 't-',
  prefixUtils = 'u-',
} = {}) {
  Vue.component(prefix + Caption.name, Caption);
  Vue.component(prefix + Inliner.name, Inliner);
  Vue.component(prefix + Link.name, Link);
  Vue.component(prefix + Avatar.name, Avatar);
  Vue.component(prefix + AspectRatio.name, AspectRatio);
  Vue.component(prefix + AspectRatioBody.name, AspectRatioBody);
  Vue.component(prefix + Panel.name, Panel);
  Vue.component(prefix + PanelElement.name, PanelElement);
  Vue.component(prefix + Section.name, Section);
  Vue.component(prefix + SectionElement.name, SectionElement);
  Vue.component(prefix + Table.name, Table);
  Vue.component(prefix + TableCaption.name, TableCaption);
  Vue.component(prefix + TableRow.name, TableRow);
  Vue.component(prefix + TableCell.name, TableCell);

  Vue.component(prefixType + Content.name, Content);
  Vue.component(prefixType + Words.name, Words);

  Vue.component(prefixUtils + Display.name, Display);
  Vue.component(prefixUtils + SpreadedInteractor.name, SpreadedInteractor);
}

export * from './components';
export { tag, el, inner } from './utils';
export default {
  install,
};
