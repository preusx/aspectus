# Vue Dropzone uploader component

File uploader component for vue. Based on `@aspectus/vue-dropzone` component.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-dropzone-uploader.html)
