/* eslint-disable prefer-object-spread, no-return-assign, no-param-reassign */
import { Dropzone } from '@aspectus/vue-dropzone';
import { FileObject, FileDescriptor, ProgressDescriptor } from './file';

const READY = 'ready';
const PROGRESS = 'progress';
const FAILURE = 'failure';

export default {
  name: 'vue-dropzone-uploader',
  FileObject,
  FileDescriptor,
  ProgressDescriptor,

  props: {
    autoload: {
      type: Boolean,
      default: false,
    },
    resource: {
      required: true,
    },
  },

  data() {
    this.progressMap = {};
    this.uidPrefix = `dropzone-uploader-file-identifier-${Date.now()}-`;
    this.id = 1;

    return {
      accepted: [],
      rejected: [],
    };
  },

  methods: {
    open() {
      this.$refs.dropzone.open();
    },
    getUid() {
      return this.uidPrefix + (++this.id);
    },
    makeFile(file, status = READY) {
      return new this.$options.FileDescriptor(
        new this.$options.FileObject(file), this.getUid(), status
      );
    },
    makeProgress(a, b) {
      return new this.$options.ProgressDescriptor(a, b);
    },
    finish(file) {
      return result => {
        file.result = result;

        this.$emit('upload', {
          result, descriptor: file,
        });
      };
    },
    reject(descriptor) {
      this.$emit('reject', { descriptor });
    },
    catch(file) {
      return result => {
        file.status = FAILURE;
        file.error = result;
        file.progress = this.makeProgress(1, 1);
      };
    },
    progress(file, { loaded, total }) {
      if (!this.progressMap[file.id]) {
        return;
      }

      file.progress = this.makeProgress(loaded, total);
    },
    upload(file, data = {}) {
      const proposal = this.resource
        .execute(
          Object.assign({}, data),
          Object.assign({ file: file.file.file }, data)
        )
        .then(this.finish(file))
        .catch(this.catch(file));

      file.status = PROGRESS;
      this.progressMap[file.id] = proposal;

      if (proposal.progress) {
        proposal.progress(event => this.progress(file, event));
      }
    },
    remove(file) {
      const { id } = file;
      const proposal = this.progressMap[id];
      if (proposal && proposal.cancel) {
        proposal.cancel();
      }

      const index = this.accepted.findIndex(x => x.id === id);
      if (index !== -1) {
        this.accepted.splice(index, 1);
      }

      delete this.progressMap[id];
      return true;
    },
    drop({ accepted, rejected }) {
      const files = accepted.map(file => this.makeFile(file, READY));
      const failed = rejected.map(file => this.makeFile(file, FAILURE));
      this.rejected = this.rejected.concat(failed);
      this.accepted = this.accepted.concat(files);

      failed.forEach(this.reject);
      if (this.autoload) {
        files.forEach(this.upload);
      }
    },
  },

  render(h) {
    return h(Dropzone, {
      ref: 'dropzone',
      scopedSlots: Object.assign({}, this.$scopedSlots, {
        default: props => this.$scopedSlots.default(Object.assign({
          accepted: this.accepted,
          rejected: this.rejected,
          upload: this.upload,
          remove: this.remove,
        }, props)),
      }),
      attrs: this.$attrs,
      on: Object.assign({ drop: this.drop }, this.$listeners),
    });
  },
};
