import { EQ_DEFAULT_BREAKPOINTS } from '@aspectus/eq';
import { Directive } from './lib';

export function install(Vue, {
  name = 'eq',
  defaultBreakpoints = EQ_DEFAULT_BREAKPOINTS,
} = {}) {
  Vue.directive(name, new Directive({ breakpoints: defaultBreakpoints }));
}

export default install;
export * from './lib';
