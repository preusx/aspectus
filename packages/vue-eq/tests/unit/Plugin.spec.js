import { createLocalVue } from '@vue/test-utils';
import Plugin from '@/main';

describe('Plugin', () => {
  it('Installs silently', () => {
    const localVue = createLocalVue();

    expect(() => localVue.use(Plugin)).not.toThrow();
  });

  it('Installs directives', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin);

    expect(localVue.options.directives.eq).toBeDefined();
  });

  it('Installs custom named directives', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { name: 'u-eq' });

    expect(localVue.options.directives['u-eq']).toBeDefined();
  });

  it('Installs custom config', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { defaultBreakpoints: ['gt1'] });

    expect(localVue.options.directives.eq).toHaveProperty('breakpoints', ['gt1']);
  });
});
