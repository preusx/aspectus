import Vue from 'vue';
import '@dermis/dermis';
import Tag from '@aspectus/vue-tag';

import Plugin from '@/main';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.use(Tag);
Vue.use(Plugin);

new Vue({
  render: h => h(App),
}).$mount('#app');
