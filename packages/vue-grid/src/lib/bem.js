import { createBlockGenerator } from '@aspectus/bem';

export const BEM_CONFIG = { n: 'g-' };
export const b = createBlockGenerator(BEM_CONFIG);
