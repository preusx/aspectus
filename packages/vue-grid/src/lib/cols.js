import flatten from 'ramda/src/flatten';

import { b } from './bem';

export const block = b('cols');
export const normalizerRegex = /\s/img;

export function normalizePassedTypes(elements) {
  return flatten(
    flatten(elements)
      .filter(x => !!x)
      .map(x => x.split(normalizerRegex))
  );
}

export function cols(...types) {
  return block(normalizePassedTypes(types).reduce((acc, type) => {
    acc[type] = true;
    return acc;
  }, {}));
}
