import { mergeContext, makeF } from '@aspectus/vue-utils';
import { Tag } from '@aspectus/vue-tag';
import { prepareProps, prepareResolver } from '@aspectus/vue-bem-styled-tag';
import { StylingPropsMixin } from '@aspectus/vue-bem-styling';

import { b } from '../bem';
import { cols } from '../cols';

export const block = b('cell');
const options = { modifiers: { justify: 'justify' } };
const resolver = prepareResolver(options);

export default makeF(
  (h, context) => {
    const { cols: c } = context.props;
    const [modifiers] = resolver(context.props);

    return h(
      Tag,
      mergeContext(context.data, {
        class: [block(modifiers), c && cols(c) || null],
      }),
      context.children
    );
  },
  Object.assign(prepareProps(options), { cols: {} }),
  'cell',
  { mixins: [StylingPropsMixin] }
);
