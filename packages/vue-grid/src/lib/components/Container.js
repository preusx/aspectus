import { generator } from '@aspectus/vue-bem-styled-tag';
import { Tag } from '@aspectus/vue-tag';
import { b } from '../bem';

export const block = b('container');

export default Object.assign(
  generator({ tag: Tag, block, modifiers: { space: 'space' } }),
  { name: 'container' }
);
