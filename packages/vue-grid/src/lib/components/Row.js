import { generator } from '@aspectus/vue-bem-styled-tag';
import { mapSame } from '@aspectus/vue-bem-styling';
import { Tag } from '@aspectus/vue-tag';
import { b } from '../bem';

export const block = b('row');

export default Object.assign(
  generator({
    tag: Tag,
    block,
    modifiers: mapSame('justify', 'content', 'space'),
  }),
  { name: 'row' }
);
