import { Cell } from '@/main';
import { sm } from './utils';

describe('Cell', () => {
  it('Mounts', () => {
    expect(sm(Cell).contains('div')).toBe(true);
    expect(sm(Cell, { propsData: { tag: 'span' } }).contains('span')).toBe(true);
  });

  it('Classes check', () => {
    const cellBase = sm(Cell);

    expect(cellBase.classes()).toContain('g-cell');
    expect(cellBase.classes()).toHaveLength(1);

    const cellCustomClasses = sm(Cell, {
      context: {
        class: ['one', { two: true, three: false }],
      },
    });

    expect(cellCustomClasses.classes()).toContain('g-cell');
    expect(cellCustomClasses.classes()).toContain('one');
    expect(cellCustomClasses.classes()).toContain('two');
    expect(cellCustomClasses.classes()).not.toContain('three');
    expect(cellCustomClasses.classes()).toHaveLength(3);
  });

  it('Modifiers check', () => {
    const cellBase = sm(Cell, {
      propsData: {
        align: 'center',
      },
    });

    expect(cellBase.classes()).toContain('g-cell');
    expect(cellBase.classes()).toContain('g-cell--align_center');
    expect(cellBase.classes()).not.toContain('g-cols');
    expect(cellBase.classes()).toHaveLength(2);

    const cellCustomClasses = sm(Cell, {
      context: {
        class: ['one', { two: true, three: false }],
      },
      propsData: {
        justify: ['center', 'left-lg'],
        align: ['center', 'left-md'],
        cols: '12 6-md 4-lg',
      },
    });

    expect(cellCustomClasses.classes()).toContain('g-cell');
    expect(cellCustomClasses.classes()).toContain('g-cell--justify_center');
    expect(cellCustomClasses.classes()).toContain('g-cell--justify_left-lg');
    expect(cellCustomClasses.classes()).toContain('g-cell--align_center');
    expect(cellCustomClasses.classes()).toContain('g-cell--align_left-md');
    expect(cellCustomClasses.classes()).toContain('g-cols');
    expect(cellCustomClasses.classes()).toContain('g-cols--12');
    expect(cellCustomClasses.classes()).toContain('g-cols--6-md');
    expect(cellCustomClasses.classes()).toContain('g-cols--4-lg');
    expect(cellCustomClasses.classes()).toContain('one');
    expect(cellCustomClasses.classes()).toContain('two');
    expect(cellCustomClasses.classes()).not.toContain('three');
    expect(cellCustomClasses.classes()).toHaveLength(11);
  });
});
