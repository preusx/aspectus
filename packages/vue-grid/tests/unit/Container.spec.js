import { Container } from '@/main';
import { sm } from './utils';

describe('Container', () => {
  it('Mounts', () => {
    expect(sm(Container).contains('div')).toBe(true);
    expect(sm(Container, { propsData: { tag: 'span' } }).contains('span')).toBe(true);
  });

  it('Classes check', () => {
    const containerBase = sm(Container);

    expect(containerBase.classes()).toContain('g-container');
    expect(containerBase.classes()).toHaveLength(1);

    const containerCustomClasses = sm(Container, {
      context: {
        class: ['one', { two: true, three: false }],
      },
    });

    expect(containerCustomClasses.classes()).toContain('g-container');
    expect(containerCustomClasses.classes()).toContain('one');
    expect(containerCustomClasses.classes()).toContain('two');
    expect(containerCustomClasses.classes()).not.toContain('three');
    expect(containerCustomClasses.classes()).toHaveLength(3);
  });

  it('Modifiers check', () => {
    const containerBase = sm(Container, {
      propsData: {
        fit: 'md',
      },
    });

    expect(containerBase.classes()).toContain('g-container');
    expect(containerBase.classes()).toContain('g-container--fit_md');
    expect(containerBase.classes()).toHaveLength(2);

    const containerCustomClasses = sm(Container, {
      context: {
        class: ['one', { two: true, three: false }],
      },
      propsData: {
        fit: ['md', 'lg-lg'],
        space: ['md', 'sm-md'],
      },
    });

    expect(containerCustomClasses.classes()).toContain('g-container');
    expect(containerCustomClasses.classes()).toContain('g-container--fit_md');
    expect(containerCustomClasses.classes()).toContain('g-container--fit_lg-lg');
    expect(containerCustomClasses.classes()).toContain('g-container--space_md');
    expect(containerCustomClasses.classes()).toContain('g-container--space_sm-md');
    expect(containerCustomClasses.classes()).toContain('one');
    expect(containerCustomClasses.classes()).toContain('two');
    expect(containerCustomClasses.classes()).not.toContain('three');
    expect(containerCustomClasses.classes()).toHaveLength(7);
  });
});
