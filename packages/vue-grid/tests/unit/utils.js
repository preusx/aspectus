import { shallowMount, createLocalVue } from '@vue/test-utils';
import TagPlugin from '@aspectus/vue-tag';

export const lv = () => {
  const localVue = createLocalVue();
  localVue.use(TagPlugin);

  return localVue;
};

export const sm = (component, config = {}) => shallowMount(
  component, { localVue: lv(), ...config }
);
