# Vue loading state mixin

Vue mixin to control internal loading state.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-loading-state-mixin.html)
