# Vue permission checker components

Components for permissions usage in vue.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-permissions.html)
