import { RenderSlot } from './lib';

function install(Vue, { name = RenderSlot.name } = {}) {
  Vue.component(name, RenderSlot);
}

export default { install };
export * from './lib';
