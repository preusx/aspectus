# Vue route installer

Vue utility to lazily install page/route related plugins.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-route-installer.html)
