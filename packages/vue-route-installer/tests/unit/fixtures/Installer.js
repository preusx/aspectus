function install(Vue) {
  Vue.component('installed-component', {
    render: h => h('h1', null, ['Installed']),
  });
}

export default { install };
