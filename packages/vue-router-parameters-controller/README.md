# Vue router parameters change controller

Utility component, that handles current route parameters change and url synchronization. Useful, for example, for a page filtering mechanism.

[Documentation](https://preusx.gitlab.io/aspectus/packages/vue-router-parameters-controller.html)
