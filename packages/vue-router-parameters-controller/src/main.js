import RouterParametersController from './RouterParametersController';

function install(Vue, {
  name = RouterParametersController.name,
} = {}) {
  Vue.component(name, RouterParametersController);
}

export {
  RouterParametersController,
};

export default {
  install,
};
