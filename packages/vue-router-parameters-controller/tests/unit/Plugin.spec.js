import { createLocalVue } from '@vue/test-utils';
import RouterParametersController from '@/RouterParametersController';
import Plugin from '@/main';

describe('Plugin', () => {
  it('Installs silently', () => {
    const localVue = createLocalVue();

    expect(() => localVue.use(Plugin)).not.toThrow();
  });

  it('Installs RouterParametersController', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin);

    expect(localVue.options.components[RouterParametersController.name]).toBeDefined();
  });

  it('Installs custom named RouterParametersController', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { name: 'v-r-controller' });

    expect(localVue.options.components[RouterParametersController.name]).not.toBeDefined();
    expect(localVue.options.components['v-r-controller']).toBeDefined();
  });
});
