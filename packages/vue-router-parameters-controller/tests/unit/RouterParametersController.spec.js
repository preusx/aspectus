import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

import Plugin from '@/main';
import RouterParametersController from '@/RouterParametersController';
import ParametersDisplay from './fixtures/ParametersDisplay.vue';
import ParametersChange from './fixtures/ParametersChange.vue';

async function mountComponent(Component, path = '/', url = path, name = 'main') {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  localVue.use(Plugin);

  const router = new VueRouter({
    routes: [{ name, path, component: Component }],
  });
  const wrapper = mount(
    { template: '<div><router-view /></div>' }, { localVue, router }
  );

  router.push(url);
  await wrapper.vm.$nextTick();

  return { wrapper, localVue, router };
}

describe('Router parameters controller', () => {
  it('Mounts', async () => {
    const Component = {
      render() {
        return (
          <router-parameters-controller>
            <span></span>
          </router-parameters-controller>
        );
      },
    };

    const { wrapper } = await mountComponent(Component);

    expect(wrapper.contains('span')).toBe(true);
  });

  it('Displays current parameters', async () => {
    const { wrapper } = await mountComponent(
      ParametersDisplay, '/params/:foo/:bar?', '/params/current/?some=one'
    );

    expect(wrapper.contains('span')).toBe(true);

    const parameters = JSON.parse(wrapper.find('span').text());

    expect(parameters).toHaveProperty('foo', 'current');
    expect(parameters).toHaveProperty('some', 'one');
    expect(parameters).not.toHaveProperty('bar');

    const { wrapper: wrapper2 } = await mountComponent(
      ParametersDisplay, '/params/:foo/:bar?', '/params/current/third/?some=one'
    );

    expect(wrapper2.contains('span')).toBe(true);

    const parameters2 = JSON.parse(wrapper2.find('span').text());

    expect(parameters2).toHaveProperty('foo', 'current');
    expect(parameters2).toHaveProperty('some', 'one');
    expect(parameters2).toHaveProperty('bar', 'third');
  });

  it('Change parameters - update url', async () => {
    const { wrapper } = await mountComponent(
      ParametersChange, '/params/:foo/:bar?', '/params/current/?some=one'
    );

    const controller = wrapper.find(RouterParametersController);

    expect(controller.vm.parameters).toHaveProperty('foo', 'current');
    expect(controller.vm.parameters).toHaveProperty('some', 'one');

    wrapper.find('button').trigger('click');
    await wrapper.vm.$nextTick();

    expect(controller.vm.parameters).toHaveProperty('foo', 'changed');
    expect(controller.vm.parameters).toHaveProperty('different', 'exists');
    expect(controller.vm.parameters).not.toHaveProperty('some', 'one');
  });
});
