import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

import Plugin from '@/main';
import RouterParametersController from '@/RouterParametersController';
import ParametersChange from './fixtures/ParametersChange.vue';

async function mountComponent(Component, path = '/', url = path, name = 'main') {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  localVue.use(Plugin);

  const router = new VueRouter({
    routes: [{ name, path, component: Component }],
  });
  const wrapper = mount(
    { template: '<div><router-view /></div>' }, { localVue, router }
  );

  router.push(url);
  await wrapper.vm.$nextTick();

  return { wrapper, localVue, router };
}

describe('Router parameters controller', () => {
  it('Life cycle events emitted', async () => {
    const ParametersChange2 = { ...ParametersChange };
    const { wrapper } = await mountComponent(
      ParametersChange2, '/params/:foo/:bar?', '/params/current/?some=one'
    );

    const controller = wrapper.find(RouterParametersController);

    wrapper.find('button').trigger('click');

    await wrapper.vm.$nextTick();

    const events = controller.emittedByOrder();

    expect(events).toBeTruthy();
    expect(events).toHaveLength(4);

    const [route1, initial, route2, internal] = events;
    expect(initial.name).toBe('change:initial');
    expect(initial.args[0]).toBe(route1.args[0]);
    expect(initial.args[0].parameters).toHaveProperty('foo', 'current');
    expect(initial.args[0].parameters).toHaveProperty('some', 'one');
    expect(initial.args[0].parameters).not.toHaveProperty('different');

    expect(internal.name).toBe('change:internal');
    expect(internal.args[0]).toBe(route2.args[0]);
    expect(internal.args[0].parameters).toHaveProperty('foo', 'changed');
    expect(internal.args[0].parameters).toHaveProperty('different', 'exists');
    expect(internal.args[0].parameters).not.toHaveProperty('some');
  });
});
