import { RouteAccessState, RouteAvailable } from './lib';

function install(Vue, {
  stateName = RouteAccessState.name,
  availableName = RouteAvailable.name,
} = {}) {
  Vue.component(stateName, RouteAccessState);
  Vue.component(availableName, RouteAvailable);
}

export default { install };
export * from './lib';
