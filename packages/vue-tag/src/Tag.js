import { internalPropsMagicTransform } from '@aspectus/vue-utils';

export default {
  name: 'tag',
  functional: true,
  props: {
    tag: {
      default: 'div',
    },
    enableVueTagPropsFix: Boolean,
  },
  render(h, context) {
    const { tag, enableVueTagPropsFix } = context.props;

    return h(
      tag,
      enableVueTagPropsFix
        ? internalPropsMagicTransform(tag, context.data)
        : context.data,
      context.children
    );
  },
};
