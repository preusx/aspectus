import Tag from './Tag';

export function install(Vue, { name = Tag.name } = {}) {
  Vue.component(name, Tag);
}

export { Tag };
export default { install };
