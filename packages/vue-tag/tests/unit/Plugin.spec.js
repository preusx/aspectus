import { createLocalVue } from '@vue/test-utils';
import Tag from '@/Tag';
import Plugin from '@/main';

describe('Plugin', () => {
  it('Installs silently', () => {
    const localVue = createLocalVue();

    expect(() => localVue.use(Plugin)).not.toThrow();
  });

  it('Installs Tag', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin);

    expect(localVue.options.components[Tag.name]).toBeDefined();
  });

  it('Installs custom named Tag', () => {
    const localVue = createLocalVue();

    localVue.use(Plugin, { name: 'v-tag' });

    expect(localVue.options.components[Tag.name]).not.toBeDefined();
    expect(localVue.options.components['v-tag']).toBeDefined();
  });
});
