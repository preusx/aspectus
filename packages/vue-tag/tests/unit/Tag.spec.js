import { shallowMount, createLocalVue } from '@vue/test-utils';
import Tag from '@/Tag';

function getH1() {
  return {
    render(h) {
      return h('h1', {
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default);
    },
  };
}

function getH2() {
  return {
    render(h) { // eslint-disable-line no-unused-vars
      return (
        <h2>
          {this.$scopedSlots.default()}
          {this.$scopedSlots.named && this.$scopedSlots.named({ some: 'thing' }) || null}
        </h2>
      );
    },
  };
}

describe('Tag', () => {
  it('Mounts', () => {
    expect(shallowMount(Tag).contains('div')).toBe(true);
  });

  it('Mounts with custom tag', () => {
    const wrapper = shallowMount(Tag, {
      propsData: {
        tag: 'span',
      },
    });

    expect(wrapper.contains('span')).toBe(true);
    expect(wrapper.contains('div')).toBe(false);
  });

  it('Mounts with custom component', () => {
    const wrapper = shallowMount(Tag, {
      propsData: {
        tag: getH1(),
      },
    });

    expect(wrapper.contains('h1')).toBe(true);
    expect(wrapper.contains('div')).toBe(false);
  });

  it('Mounts with custom registered component', () => {
    const localVue = createLocalVue();
    localVue.component('comp', getH1());

    const wrapper = shallowMount(Tag, {
      localVue,
      propsData: {
        tag: 'comp',
      },
    });

    expect(wrapper.contains('h1')).toBe(true);
    expect(wrapper.contains('div')).toBe(false);
  });

  it('Mounts with slots', () => {
    const localVue = createLocalVue();
    localVue.component('comp', getH1());
    localVue.component('comp2', getH2());

    const wrapper = shallowMount(Tag, {
      localVue,
      propsData: {
        tag: 'comp',
      },
      slots: {
        default: '<span />',
      },
    });

    expect(wrapper.contains('h1')).toBe(true);
    expect(wrapper.contains('span')).toBe(true);

    const wrapper2 = shallowMount(Tag, {
      localVue,
      propsData: {
        tag: 'comp2',
      },
      slots: {
        default: '<span />',
        named: '<h3 />',
      },
    });

    expect(wrapper2.contains('h2')).toBe(true);
    expect(wrapper2.contains('span')).toBe(true);
    expect(wrapper2.contains('h3')).toBe(true);
  });
});
