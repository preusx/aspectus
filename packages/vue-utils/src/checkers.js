/* eslint-disable import/prefer-default-export */
export function isEmpty(value) {
  return (
    value === null ||
    value === '' ||
    typeof value === 'undefined' ||
    (Array.isArray(value) && value.length === 0) ||
    (Object(value) === value && Object.keys(value).length === 0)
  );
}
