/* eslint-disable import/prefer-default-export, no-underscore-dangle, prefer-object-spread */
import curry from 'ramda/src/curry';
import { getProps } from './props';
import { makeF } from './components';
import { mergeContext } from './render';

export const withDefaultProps = curry((defaults, Component) => makeF(
  (h, context) => h(
    Component,
    mergeContext(context.data, { attrs: context.props }),
    context.children
  ),
  Object.keys(defaults).reduce(
    (acc, key) => {
      acc[key] = { default: defaults[key] };

      return acc;
    },
    getProps(Component)
  )
));
