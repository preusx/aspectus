import propsMergerFunction from '@vue/babel-helper-vue-jsx-merge-props';

export function mergeContext(...attrs) {
  return propsMergerFunction(attrs);
}

export function renderSlim(nodes, h, tag = 'div', data) {
  if (nodes.length === 0) {
    return null;
  }

  if (nodes.length === 1) {
    return nodes[0];
  }

  return h(tag, data, nodes);
}
