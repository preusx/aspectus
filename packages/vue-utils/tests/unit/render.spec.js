import { shallowMount } from '@vue/test-utils';
import { renderSlim, mergeContext } from '@/main';

const Element = name => ({ render: h => h(name) });

describe('Slim renderer', () => {
  const Functional = {
    functional: true,
    render(h, context) {
      return renderSlim(context.children, h, 'div', context.data);
    },
  };

  const Stateful = {
    render(h) {
      return renderSlim(this.$scopedSlots.default({}), h, 'div');
    },
  };

  const testType = Type => () => {
    const wrapper = shallowMount(Type, {
      slots: { default: [Element('span')] },
    });

    expect(wrapper.is('span')).toBe(true);
    expect(wrapper.is('div')).toBe(false);

    const wrapper2 = shallowMount(Type, {
      slots: { default: [Element('h1'), Element('h2')] },
    });

    expect(wrapper2.contains('h1')).toBe(true);
    expect(wrapper2.is('h1')).toBe(false);
    expect(wrapper2.contains('h2')).toBe(true);
    expect(wrapper2.is('div')).toBe(true);
  };

  it('Functional renderer', testType(Functional));
  it('Stateful renderer', testType(Stateful));
});

describe('Context merger', () => {
  const createContextRenderer = ctx => ({
    functional: true,
    render(h, context) {
      return h('div', mergeContext(context.data, ctx), context.children);
    },
  });

  it('Classes merge', () => {
    const Component = createContextRenderer({ class: 'some-class' });

    const wrapper = shallowMount(
      { template: '<ccmp class="other-class" />' }, {
        stubs: {
          ccmp: Component,
        },
      }
    );
    const classes = wrapper.classes();

    expect(wrapper.is('div')).toBe(true);
    expect(classes).toContain('other-class');
    expect(classes).toContain('some-class');

    const wrapper2 = shallowMount(
      { template: '<ccmp :class="\'other-class\'" />' }, {
        stubs: {
          ccmp: Component,
        },
      }
    );
    const classes2 = wrapper2.classes();

    expect(wrapper2.is('div')).toBe(true);
    expect(classes2).toContain('other-class');
    expect(classes2).toContain('some-class');

    const Component2 = createContextRenderer({
      class: ['some-class', { active: true }],
    });

    const wrapper3 = shallowMount(
      { template: '<ccmp :class="[\'other-class\', { close: false }]" />' }, {
        stubs: {
          ccmp: Component2,
        },
      }
    );
    const classes3 = wrapper3.classes();

    expect(wrapper3.is('div')).toBe(true);
    expect(classes3).toContain('other-class');
    expect(classes3).toContain('active');
    expect(classes3).not.toContain('close');
    expect(classes3).toContain('some-class');
  });
});
